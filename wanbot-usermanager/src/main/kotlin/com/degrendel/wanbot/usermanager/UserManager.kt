package com.degrendel.wanbot.usermanager

import com.degrendel.wanbot.common.Session
import com.degrendel.wanbot.common.WanbotPlugin
import com.degrendel.wanbot.common.inputs.Avatar
import com.degrendel.wanbot.common.logger
import com.google.gson.Gson
import java.io.InputStreamReader

class UserManager : WanbotPlugin
{
  companion object
  {
    val L by logger()
  }

  private val userData: List<UserData>

  init
  {
    val handle = InputStreamReader(UserManager::class.java.getResourceAsStream("/user-manager/users.json"))
    userData = Gson().fromJson(handle, Array<UserData>::class.java).toList()
    L.info("LOADED {} USERS FROM JSON", userData.size)
  }

  override fun name() = "User Manager"

  override fun start(session: Session)
  {
    for (entry in userData)
    {
      val user = session.input.getUser(entry.userId)
      entry.names.forEachIndexed { i, name -> if (i == 0) user.addAlias(name, true) else user.addAlias(name) }
      for (avatar in entry.accounts)
        session.input.addAvatar(Avatar(userId = entry.userId, serviceId = avatar.serviceId, memberId = avatar.memberId))
    }
  }

  override fun stop()
  {
  }
}
