package com.degrendel.wanbot.usermanager

data class UserData(val userId: String, val names: List<String>, val accounts: List<AvatarData>)

data class AvatarData(val serviceId: String, val memberId: String)
