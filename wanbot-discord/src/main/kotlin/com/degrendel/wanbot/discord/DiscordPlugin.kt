package com.degrendel.wanbot.discord

import com.degrendel.wanbot.common.*
import com.degrendel.wanbot.common.inputs.Constants
import com.degrendel.wanbot.common.inputs.InputManager
import com.degrendel.wanbot.common.inputs.Server
import com.degrendel.wanbot.common.inputs.Service
import discord4j.core.DiscordClientBuilder
import discord4j.core.`object`.entity.Member
import discord4j.core.`object`.entity.MessageChannel
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.util.Snowflake
import discord4j.core.event.domain.PresenceUpdateEvent
import discord4j.core.event.domain.channel.TextChannelCreateEvent
import discord4j.core.event.domain.guild.GuildCreateEvent
import discord4j.core.event.domain.lifecycle.ReadyEvent
import discord4j.core.event.domain.message.MessageCreateEvent
import java.io.File
import java.nio.charset.Charset
import java.time.Instant
import java.util.concurrent.atomic.AtomicBoolean

class DiscordPlugin : WanbotPlugin
{
  companion object
  {
    val L by logger()
    const val SERVICE_ID = "discord"
    // TODO: Assert this can't conflict with 'real' ids
    const val PRIVATE_ID = "private"
  }

  inner class Client(private val session: Session, discordKey: String) : Runnable, TextOutputRelay, ServiceLocation
  {
    override val serviceId = SERVICE_ID

    private val client = DiscordClientBuilder(discordKey).build()
    private val connected = AtomicBoolean(false)

    private val magicCommands = if (session.config.c.getBoolean("discord.allow-all-magic"))
      InputManager.MagicCommands.values().toSet()
    else
      setOf(InputManager.MagicCommands.PING, InputManager.MagicCommands.GRAMMAR)

    init
    {
      client.eventDispatcher.on(ReadyEvent::class.java).subscribe { e -> onReadyEvent(e) }
      client.eventDispatcher.on(GuildCreateEvent::class.java).subscribe { e -> onGuildCreateEvent(e) }
      client.eventDispatcher.on(TextChannelCreateEvent::class.java).subscribe { e -> onTextChannelCreateEvent(e) }
      client.eventDispatcher.on(PresenceUpdateEvent::class.java).subscribe { e -> onPresenceUpdateEvent(e) }
      client.eventDispatcher.on(MessageCreateEvent::class.java).subscribe { e -> onMessageCreateEvent(e) }
      session.registerTextOutputRelay(SERVICE_ID, this)
      // TODO: What other events would be interesting?
    }

    private fun onMessageCreateEvent(event: MessageCreateEvent)
    {
      if (!event.message.content.isPresent)
      {
        L.warn("GOT DISCORD MESSAGE W/O CONTENT!? $event ${event.message}")
        return
      }
      if (!event.message.author.isPresent)
      {
        L.info("GOT DISCORD MESSAGE W/O AN AUTHOR? $event")
        return
      }
      if (!client.selfId.isPresent)
      {
        L.warn("GOT MESSAGE BUT DON'T HAVE A SELF ID?")
        return
      }
      val self = (client.selfId.get() == event.message.author.get().id)
      val reference = Instant.now()
      val timestamp = if (event.message.timestamp > reference)
      {
        L.warn("GOT MESSAGE FROM THE FUTURE ({})", event.message.timestamp.minusMillis(reference.toEpochMilli()).toEpochMilli())
        reference
      }
      else reference
      val memberId = event.message.author.get().id.asString()
      val serverId = if (event.guildId.isPresent)
        event.guildId.get().asString()
      else
        PRIVATE_ID
      val location = object : MessageLocation
      {
        override val serviceId = SERVICE_ID
        override val serverId = serverId
        override val channelId = event.message.channelId.asString()
        override val memberId = memberId
      }
      session.input.addMessage(location, event.message.content.get(), self, timestamp, magicCommands)
    }

    private fun onPresenceUpdateEvent(event: PresenceUpdateEvent)
    {
      L.info("PRESENCE UPDATE ${event.guildId.asString()} :: ${event.userId.asString()} ${event.current.status.value}")
    }

    override fun speak(location: ChannelLocation, message: String)
    {
      // TODO: We should probably cache the channel objects, since apparently this is expensive
      // TODO: is block() ok here?
      // TODO: will it be MessageChannel or TextChannel?
      client.getChannelById(Snowflake.of(location.channelId)).subscribe {
        if (it !is MessageChannel)
          L.warn("Attempted to lookup ${location.channelId}, got a not message channel $it")
        else
          it.createMessage(message).block()
      }
    }

    private fun onReadyEvent(event: ReadyEvent)
    {
      L.warn("WANBOT IS CONNECTED TO DISCORD")
      connected.set(true)
      val service = session.input.getService(this)
      service.name = "Discord"
    }

    private fun onGuildCreateEvent(event: GuildCreateEvent)
    {
      val guild = event.guild
      L.warn("WANBOT IS IN ${guild.name}")
      val location = object : ServerLocation
      {
        override val serviceId = SERVICE_ID
        override val serverId = guild.id.asString()
      }
      val server = session.input.getServer(location)
      server.name = guild.name

      guild.channels.filter { it is TextChannel }.subscribe { newTextChannel(it as TextChannel) }
      guild.members.subscribe { newMember(it) }
    }

    private fun onTextChannelCreateEvent(event: TextChannelCreateEvent) = newTextChannel(event.channel)

    private fun newTextChannel(channel: TextChannel)
    {
      L.info("WANBOT IS TXTING IN ${channel.name}")
      val location = object : ChannelLocation
      {
        override val serviceId = SERVICE_ID
        override val serverId = channel.guildId.asString()
        override val channelId = channel.id.asString()
      }
      val channelInput = session.input.getChannel(location)
      channelInput.name = channel.name
      channelInput.metadata["private"] = Constants.FALSE
    }

    private fun newMember(member: Member)
    {
      val location = object : MemberLocation
      {
        override val serviceId = SERVICE_ID
        override val serverId = member.guildId.asString()
        override val memberId: String = member.id.asString()
      }
      val memberInput = session.input.getMember(location)
      memberInput.name = member.displayName
      memberInput.onAllChannels = true
    }

    override fun run()
    {
      client.login().block()
    }

    fun terminate()
    {
      if (connected.get())
        client.logout()
    }
  }

  private var client: Client? = null

  override fun name() = "Discord Interface"

  override fun start(session: Session)
  {
    if (!session.config.c.getBoolean("discord.enable"))
    {
      L.info("DISCORD ENABLE SET TO FALSE, NOT STARTING DISCORD CONNECTION")
      return
    }
    val discordKeySource = session.config.c.getString("discord.api-key-source")
    val discordKey = when (discordKeySource)
    {
      "file" ->
      {
        val discordKeyFilename = session.config.c.getString("discord.api-key-filename")
        L.info("READING DISCORD KEY FROM {}", discordKeyFilename)
        File(discordKeyFilename).readText(Charset.defaultCharset()).trim()
      }
      "env" ->
      {
        val discordKeyEnv = session.config.c.getString("discord.api-key-env")
        L.info("READING DISCORD KEY FROM \$env:{}", discordKeyEnv)
        System.getenv(discordKeyEnv)
      }
      else -> throw IllegalStateException("Invalid discord.api-key-source value of $discordKeySource")
    }
    if (discordKey.isBlank())
      throw IllegalStateException("Invalid discord key sourced from $discordKeySource")
    client = Client(session, discordKey)
    Thread(client).start()
  }

  override fun stop()
  {
    client?.terminate()
  }
}
