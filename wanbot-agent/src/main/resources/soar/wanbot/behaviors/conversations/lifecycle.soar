# Rules for managing the lifecycle of conversations

set SCOPE [scope-name behaviors*conversations lifecycle]

# TODO: need to record how many messages haven't applied to this conversation - feels crazy
# expensive to do one at a time (bit of an O(N^2) cost) - probably want some sort of RHS
# function or :o-support hack


sp "$SCOPE*elaborate*goal
    [ngs-match-goal-to-create-subgoal <s> $G_ROOT <super> $G_MAINTAIN_CONVERSATION_LIFECYCLE <pool>]
    [ngs-bind <s> conversations.conversation]
-->
    [ngs-create-goal-in-place <pool> $G_MAINTAIN_CONVERSATION_LIFECYCLE $NGS_GB_MAINT <g> <super> { conversation <conversation> }]
"

# Also this feels kinda expensive
sp "$SCOPE*propose*copy*start*time
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.my-type]
    [ngs-nex <conversation> start]
    [ngs-bind <s> clock.timestamp]
-->
    [ngs-create-attribute-by-operator <s> <conversation> start <timestamp> "" "> ="]
    [ngs-add-log-side-effect $SCOPE $NGS_INFO "New conversation <conversation> !<my-type> @ <timestamp>"]
"

sp "$SCOPE*monitor*conversation*done
    [ngs-match-top-state <s> conversations.conversation.my-type]
    [ngs-is-tagged <conversation> $CONVERSATION_COMPLETE]
-->
    [ngs-log $SCOPE $NGS_INFO "Conversation <conversation> !<my-type> marked as completed"]
"

sp "$SCOPE*elaborate*active
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation]
    [ngs-is-not-tagged <conversation> $CONVERSATION_COMPLETE]
-->
    [ngs-tag <conversation> $CONVERSATION_ACTIVE]
"

sp "$SCOPE*propose*mark*initial*message
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.messages.message]
    [ngs-nex <messages> initial]
-->
    [ngs-create-attribute-by-operator <s> <messages> initial <message> "" "> ="]
"

sp "$SCOPE*elaborate*channel
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.channel-id]
    [ngs-bind <s> channels.channel.channel-id]
-->
    [ngs-create-attribute <conversation> channel <channel>]
"

sp "$SCOPE*elaborate*most*recent*response
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.responses.response.timestamp]
    [ngs-not \
        (<responses> ^response [ngs-this-not-that <rnew> <response>]) \
        [ngs-gt <rnew> timestamp <timestamp>] \
    ]
-->
    [ngs-create-attribute <conversation> latest-response <timestamp>]
"

sp "$SCOPE*elaborate*latest*message
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.messages.message.timestamp]
    [ngs-not \
        (<messages> ^message [ngs-this-not-that <mnew> <message>]) \
        [ngs-gt <mnew> timestamp <timestamp>] \
    ]
-->
    [ngs-create-attribute <messages> latest <message>]
    [ngs-create-attribute <conversation> latest-message <timestamp>]
"

sp "$SCOPE*elaborate*last*active*due*to*start
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.start]
    [ngs-not \
        [ngs-or \
            [ngs-gt <conversation> latest-message <start>] \
            [ngs-gt <conversation> latest-response <start>] \
        ] \
    ]
-->
    [ngs-create-attribute <conversation> $CONVERSATION_LATEST_ACTIVITY_ATTR <start>]
"

sp "$SCOPE*elaborate*last*active*due*to*message
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.latest-message]
    [ngs-not \
        [ngs-or \
            [ngs-gt <conversation> start <latest-message>] \
            [ngs-gt <conversation> latest-response <latest-message>] \
        ] \
    ]
-->
    [ngs-create-attribute <conversation> $CONVERSATION_LATEST_ACTIVITY_ATTR <latest-message>]
"

sp "$SCOPE*elaborate*last*active*due*to*response
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.latest-response]
    [ngs-not \
        [ngs-or \
            [ngs-gt <conversation> start <latest-response>] \
            [ngs-gt <conversation> latest-message <latest-response>] \
        ] \
    ]
-->
    [ngs-create-attribute <conversation> $CONVERSATION_LATEST_ACTIVITY_ATTR <latest-response>]
"

sp "$SCOPE*monitor*last*active*time
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.$CONVERSATION_LATEST_ACTIVITY_ATTR:<timestamp>]
-->
    [ngs-log $SCOPE $NGS_INFO "Conversation <conversation> last active at <timestamp>"]
"

sp "$SCOPE*elaborate*default*most*recent*response
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation]
    [ngs-bind <conversation> responses start]
    [ngs-nex <responses> response]
-->
    [ngs-create-attribute <conversation> latest-response 0]
"

sp "$SCOPE*elaborate*have*responded
    [ngs-match-goal <s> $G_MAINTAIN_CONVERSATION_LIFECYCLE <g>]
    [ngs-bind <g> conversation.responses.response]
-->
    [ngs-tag <conversation> $CONVERSATION_HAS_RESPONSE]
"