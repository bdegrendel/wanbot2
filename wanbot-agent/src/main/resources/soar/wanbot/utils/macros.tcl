# Various macros used across the Wanbot Soar codebase

# Returns the scope name to be used in all productions in this file
proc scope-name { module filename } {
    return "wanbot*$module*$filename"
}

# Declares a goal plus global variable containing its name
proc declare-goal { var name body } {
    CORE_CreateMacroVar $var $name
    NGS_DeclareGoal $name $body
}

# Match if capability is enabled
proc is-enabled { name { state "<s>" } } {
    return [ngs-eq $state capabilities.$name $::NGS_YES]
}

# Opens a LHS structured conditional
# Expects no context to be set
proc lhs { state block } {
    upvar lhs_context parent_lhs_context
    if { [info exists parent_lhs_context] } {
        error "[lindex [info level 0] 0] expects not be in a LHS context, but we're in $parent_lhs_context"
    }

    namespace eval lhs {
        set channel_id 0
        set message_id 0
        set generic_id 0
        set user_id    0
    }
    set binding $state
    set ::lhs::state_binding $state
    set lhs_context root
    set ret [subst $block]
    namespace delete lhs
    return $ret
}

proc _expect_lhs_context { context } {
    upvar 2 lhs_context lhs_context
    if { ![info exists lhs_context] } {
        error "[lindex [info level -1] 0] expects to be in LHS context $context, but none is set (missing \[lhs...\]?)"
    } elseif { ($lhs_context ne $context) } {
        error "[lindex [info level -1] 0] expects state $context, but we're in $lhs_context"
    }
}

# Bind to a user
# Expects root context, opens user context
#
# block Unsubstituted string to execute in the child context
proc bind-user { block } {
    _expect_lhs_context root

    set ::lhs::user_id [incr ::lhs::user_id]
    set binding "<user-$::lhs::user_id>"
    set ::lhs::user_binding $binding
    set lhs_context user

    if { $::lhs::user_id == 1 } {
        set bind "[ngs-bind $::lhs::state_binding users]
                  [ngs-bind <users> user:$binding]"
    } else {
        set bind "(<users> ^user \{ $binding "
        for { set i 1 } { $i < $::lhs::user_id } { incr i } {
            set bind "$bind <> <user-$i>"
        }
        set bind "$bind \})\n"
    }

    return "$bind
            [subst $block]"
}

# Bind to a channel
# Expects root context, opens channel context
#
# block Unsubstituted string to execute in the child context
proc bind-channel { block } {
    _expect_lhs_context root

    set ::lhs::channel_id [expr $::lhs::channel_id + 1]
    set binding "<channel-$::lhs::channel_id>"
    set ::lhs::channel_binding $binding
    set lhs_context channel


    if { $::lhs::channel_id == 1 } {
        set bind "[ngs-bind $::lhs::state_binding channels]
                  [ngs-bind <channels> channel:$binding]"
    } else {
        set bind "(<channels> ^channel \{ $binding "
        for { set i 1 } { $i < $::lhs::channel_id } { incr i } {
            set bind "$bind <> <message-$i>"
        }
        set bind "$bind \})\n"
    }

    set messages_binding "<messages-$::lhs::channel_id>"
    set ::lhs::messages_binding $messages_binding

    return "$bind
            [ngs-bind $binding messages:$messages_binding]
            [subst $block]"
}

# Match if this channel is private (aka direct message)
# Expects channel context
proc channel-is-private { } {
    _expect_lhs_context channel

    upvar binding binding 
    return [ngs-eq $binding private $::NGS_YES]
}

# Match if is this channel is not private
# Expects channel context
proc channel-is-public { } {
    _expect_lhs_context channel

    upvar binding binding
    return [ngs-eq $binding private $::NGS_NO]
}

# Bind to the metadata of a channel
# Expects channel context, opens channel-metadata context
#
# block Unsubstituted string to execute in the child context
proc bind-channel-metadata { block } {
    _expect_lhs_context channel

    upvar binding channel_binding
    set binding "<metadata-$::lhs::channel_id>"
    set ::lhs::channel_metadata_binding $binding
    set lhs_context channel-metadata

    return "[ngs-bind $channel_binding metadata:$binding]
            [subst $block]"
}

# Bind to the behaviors of a channel
# Expects channel context, opens channel-behaviors context
#
# block Unsubstituted string to execute in the child context
proc bind-behaviors { block } {
    _expect_lhs_context channel

    upvar binding channel_binding
    set binding "<behaviors-$::lhs::channel_id>"
    set ::lhs::channel_behavior_binding $binding
    set lhs_context channel-behaviors

    return "[ngs-bind $channel_binding behaviors:$binding]
            [subst $block]"
}

# Bind to a message in a channel, opens message context
# Expects channel context, opens message context
#
# Note: don't mix this with [bind-root-message]
#
# block Unsubstituted string to execute in the child context
proc bind-message { block } {
    _expect_lhs_context channel

    set ::lhs::message_id [expr $::lhs::message_id + 1]
    set binding "<message-$::lhs::message_id>"
    set ::lhs::message_binding $binding
    upvar binding channel_binding
    set lhs_context message
    
    if { $::lhs::message_id == 1 } {
        set bind "[ngs-bind $::lhs::messages_binding message:$binding]"
    } else {
        set bind "($::lhs::messages_binding ^message \{ $binding "
        for { set i 1 } { $i < $::lhs::message_id } { incr i } {
            set bind "$bind <> <message-$i>"
        }
        set bind "$bind \})"
    }

    return "$bind
            [subst $block]"
}

# Bind to a message not dependant on a channel
# Expects root context, opens message context
#
# Note: don't mix this with [bind-channel {[bind-message]}]
#
# block Unsubstituted string to execute in the child context
proc bind-root-message { block } {
    _expect_lhs_context root

    set ::lhs::message_id [expr $::lhs::message_id + 1]
    set binding "<message-$::lhs::message_id>"
    set ::lhs::message_binding $binding

    if { $::lhs::message_id == 1 } {
        set ::lhs::messages_binding "<messages>"
        set bind "[ngs-bind $::lhs::state_binding messages:$::lhs::messages_binding]
                  [ngs-bind $::lhs::messages_binding message:$binding]"
    } else {
        set bind "($::lhs::messages_binding ^message \{ $binding "
        for { set i 1 } { $i < $::lhs::message_id } { incr i } {
            set bind "$bind <> <message-$i>"
        }
        set bind "$bind \})"
    }

    return "$bind
            [subst $block]"
}

# Matches if no message with the following is found
# Expects channel context, opens message context
#
# block Unsubstituted string to execute in the child context
proc no-message { block } {
    _expect_lhs_context channel

    set ::lhs::generic_id [expr $::lhs::generic_id + 1]
    set binding "<not-message-$::lhs::generic_id>"
    set ::lhs::message_binding $binding
    upvar binding channel_binding
    set lhs_context message

    return "[ngs-not \
        [ngs-bind $::lhs::messages_binding message:$binding] \
        [subst $block]
    ]"
}

# Matches if this message was not produced by Wanbot
# Expects message context
proc message-is-not-self { } {
    _expect_lhs_context message

    upvar binding binding
    return [ngs-eq $binding self $::NGS_NO]
}

# Matches if this message was produced by Wanbot
# Expects message context
proc message-is-self { } {
    _expect_lhs_context message

    upvar binding binding
    return [ngs-eq $binding self $::NGS_YES]
}

proc message-has-prompt { } {
    _expect_lhs_context message

    upvar binding binding
    return [ngs-is-tagged $binding $::MESSAGE_TAG_PROMPT]
}

proc message-is-short { } {
    _expect_lhs_context message

    upvar binding binding
    return [ngs-is-tagged $binding $::MESSAGE_TAG_SHORT]
}

proc message-is-mediu { } {
    _expect_lhs_context message

    upvar binding binding
    return [ngs-is-tagged $binding $::MESSAGE_TAG_MEDIUM]
}

proc message-is-long { } {
    _expect_lhs_context message

    upvar binding binding
    return [ngs-is-tagged $binding $::MESSAGE_TAG_LONG]
}

# Binds to a messages's parse
# Expects message context, opens message-parse context
#
# block Unsubsituted string to execute in the child context
proc bind-parse { block } {
    _expect_lhs_context message

    set binding "<parse-$::lhs::message_id>"
    variable ::lhs::message_parse_binding $binding
    upvar binding message_binding
    set lhs_context message-parse

    return "[ngs-bind $message_binding parse:$binding]
            [subst $block]"
}

# Matches if this message's parse is a specific category
# Expects message-parse context
#
# category  Category name of the parse, Soar String'd if necessary
proc parse-is-category { category } {
    _expect_lhs_context message-parse

    upvar binding binding
    return [ngs-eq $binding category $category]
}