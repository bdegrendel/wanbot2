# Glide vDEVELOPMENT
# Generated on 2019-12-14T19:53:02.647286600Z
#
# Arguments: -o wanbot/bundle.tcl wanbot
#
# Autogenerated code, do not modify

namespace eval Response {
    ::NGS_DeclareType Response {}

    namespace export create
    proc create { service-id server-id channel-id message { cooldown "" } { destination "" } { attribute "response" } { binding "<response>" } } {
        ::glide::check-rhs elaboration
        ::glide::check-types { ${service-id} String ${server-id} String ${channel-id} String ${message} String ${cooldown} Int $destination String $attribute String $binding Binding }
        ::glide::claim-binding $binding Response

        if { $destination eq "" } {
            set destination_ [::glide::output-binding]
        } else {
            set destination_ $destination
        }

        ::glide::check-i-modification $destination_ $attribute Response

        set attrs [dict create service-id ${service-id} server-id ${server-id} channel-id ${channel-id} message ${message} ]
        if { ${cooldown} ne "" } {
            dict lappend attrs cooldown ${cooldown}
        }

        return [ngs-create-typed-object $destination_ $attribute Response $binding $attrs]
    }

    namespace export create-by-operator
    proc create-by-operator { service-id server-id channel-id message { cooldown "" } { destination "" } { attribute "response" } { binding "<response>" } { state "" } { add_prefs "=" } } {
        ::glide::check-rhs proposal
        ::glide::check-types { ${service-id} String ${server-id} String ${channel-id} String ${message} String ${cooldown} Int $destination String $attribute String $binding Binding $state Binding? $add_prefs Preferences }
        ::glide::claim-binding $binding Response

        if { $state eq "" } {
            set state_ [::glide::state-binding]
        } else {
            set state_ $state
        }
        if { $destination eq "" } {
            set destination_ [::glide::output-binding]
        } else {
            set destination_ $destination
        }

        ::glide::check-o-modification $destination_ $attribute Response

        set attrs [dict create service-id ${service-id} server-id ${server-id} channel-id ${channel-id} message ${message} ]
        if { ${cooldown} ne "" } {
            dict lappend attrs cooldown ${cooldown}
        }

        return [ngs-create-typed-object-by-operator $state_ $destination_ $attribute Response $binding $attrs $::NGS_ADD_TO_SET $add_prefs]
    }

    # TODO: Other functions?
}

# Generates a checked elaboration rule
#
# Enforces that the RHS does not propose an operator.  Prepends the production
# name with $SCOPE, if set, plus elaborate.
#
# name          The production name
# docstring?    Production's docstring, optional
# lhs           LHS string block
# -->           Ignored, style word to make rules more readable
# rhs           RHS string block
proc elaboration { args } {
    # TODO: Stub!
}

# Generates a checked proposal rule
#
# Enforces that the RHS proposes an operator, but otherwise does not modify
# working memory.  Prepends the production name with $SCOPE, if set, plus
# propose.
#
# name          The production name
# docstring?    Production's docstring, optional
# lhs           LHS string block
# -->           Ignored, style word to make rules more readable
# rhs           RHS string block
proc proposal { args } {
    # TODO: Stub!
}


# Generates a checked monitor rule
#
# Enforces that the RHS neither proposes an operator, nor directly modify
# working memory.  Allows RHS functions, since they typically do not modify
# the agent, though that cannot be checked.  Prepends the production name
# with $SCOPE, if set, plus monitor.
#
# name          The production name
# docstring?    Production's docstring, optional
# lhs           LHS string block
# -->           Ignored, style word to make rules more readable
# rhs           RHS string block
proc monitor { args } {
    # TODO: Stub!
}

# Creates an unchecked production
#
# Enforces typing, but otherwise does not enforce any rules.  Go hog wild.
# Prepends the production name with $SCOPE, if set, but no other keywords.
#
# name          The production name
# docstring?    Production's docstring, optional
# lhs           LHS string block
# -->           Ignored, style word to make rules more readable
# rhs           RHS string block
proc production { args } {
    # TODO: Stub!
}

# Sets the local SCOPE variable
#
# All Glide productions, plus logging, between set-scope calls will be labeled
# as belonging to this common scope.  A typical, but not required, strategy
# would be to use the relative path and filename of the current file.
#
# Yes, ideally this would be set automagically, but JTCL is awful and doesn't
# support [info frame], so scripts can progmatically determine where they are.
# Call your congress representative and complain.
#
#
proc set-scope { keywords } {
    variable SCOPE [join $keywords *]
}

namespace eval glide {
    namespace export check-rhs
    proc check-rhs { type } {
        # TODO: Stub!
    }

    namespace export check-types
    proc check-types { spec } {
        # TODO: Stub!
    }

    namespace export claim-binding
    proc claim-binding { binding type } {
        # TODO: Stub!
    }

    namespace export check-i-modification
    proc check-i-modification { binding attribute type } {
        # TODO: Stub!
    }

    namespace export check-o-modification
    proc check-o-modification { binding attribute type } {
        # TODO: Stub!
    }

    namespace export state-binding
    proc state-binding {  } {
        # TODO: Stub!
        return "<s>"
    }

}