# Rules related to marking messages

set SCOPE [scope-name messages markings]

# TODO: Expand these to be per user or per channel
sp "$SCOPE*elaborate*previous*different*by*timestamp
    [ngs-match-top-state <s> channels.channel.messages]
    [ngs-bind <messages> message:<marker> message:<target>]
    [ngs-is-tagged <marker> $MESSAGE_FIND_PREVIOUS_DIFFERENT]
    [ngs-bind <marker> member-id timestamp]
    [ngs-neq <target> member-id <member-id>]
    [ngs-lt <target> timestamp <timestamp>]
    [ngs-bind <target> message-id timestamp:<target-timestamp>]
    [ngs-not \
        [ngs-bind <messages> message:<better>] \
        [ngs-neq <better> member-id <member-id>] \
        [ngs-gt <better> timestamp <target-timestamp>] \
        [ngs-lt <better> timestamp <timestamp>] \
    ]
-->
    [ngs-create-attribute <marker> $MESSAGE_PREVIOUS_DIFFERENT_ATTR <message-id>]
    [ngs-log $SCOPE $NGS_INFO "Found previous different message for <marker> in <target> :: <message-id>"]
"

sp "$SCOPE*elaborate*previous*different*no*match
    [ngs-match-top-state <s> channels.channel.messages]
    [ngs-bind <messages> message:<marker>]
    [ngs-is-tagged <marker> $MESSAGE_FIND_PREVIOUS_DIFFERENT]
    [ngs-bind <marker> member-id timestamp]
    [ngs-not \
        [ngs-bind <messages> message:<target>] \
        [ngs-neq <target> member-id <member-id>] \
        [ngs-lt <target> timestamp <timestamp>] \
    ]
-->
    [ngs-tag <marker> $MESSAGE_NO_PREVIOUS_DIFFERENT]
    [ngs-log $SCOPE $NGS_INFO "Didn't find a previous different message for <marker>"]
"

sp "$SCOPE*elaborate*next*same*by*timestamp
    [ngs-match-top-state <s> channels.channel.messages]
    [ngs-bind <messages> message:<marker> message:<target>]
    [ngs-is-tagged <marker> $MESSAGE_FIND_NEXT_SAME]
    [ngs-bind <marker> member-id timestamp]
    [ngs-eq <target> member-id <member-id>]
    [ngs-gt <target> timestamp <timestamp>]
    [ngs-bind <target> message-id timestamp:<target-timestamp>]
    [ngs-not \
        [ngs-bind <messages> message:<better>] \
        [ngs-eq <better> member-id <member-id>] \
        [ngs-lt <better> timestamp <target-timestamp>] \
        [ngs-gt <better> timestamp <timestamp>] \
    ]
-->
    [ngs-create-attribute <marker> $MESSAGE_NEXT_SAME_ATTR <message-id>]
    [ngs-log $SCOPE $NGS_INFO "Found next same message for <marker> in <target> :: <message-id>"]
"

sp "$SCOPE*elaborate*next*same*no*match
    [ngs-match-top-state <s> channels.channel.messages]
    [ngs-bind <messages> message:<marker>]
    [ngs-is-tagged <marker> $MESSAGE_FIND_NEXT_SAME]
    [ngs-bind <marker> member-id timestamp]
    [ngs-not \
        [ngs-bind <messages> message:<target>] \
        [ngs-eq <target> member-id <member-id>] \
        [ngs-gt <target> timestamp <timestamp>] \
    ]
-->
    [ngs-tag <marker> $MESSAGE_NO_NEXT_SAME]
    [ngs-log $SCOPE $NGS_INFO "Didn't find a next same message for <marker>"]
"