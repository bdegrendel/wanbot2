# Declare various conversation types

NGS_DeclareType Conversation ""

NGS_DeclareType Response ""

declare-conversation F_CHAIN FChain "" "F F"
declare-conversation WHERE_IS_USER WhereIsUser "" ""
declare-conversation RIP RIP "" "RIP RIP"
declare-conversation WUT Wut "" "USER_SAID UserSaid The THE"
declare-conversation PROMPT Prompt "" "CONFUSED Confused WELL_WHAT WellWhat"

# Instance tag this is an active conversation
CORE_CreateMacroVar CONVERSATION_ACTIVE active

# Instance tag this conversation has been completed, and will be removed after a set period of time
CORE_CreateMacroVar CONVERSATION_COMPLETE complete

# Instance tag indicating that at least one response has been issued
CORE_CreateMacroVar CONVERSATION_HAS_RESPONSE has*response

# Settings tag indicating all messages before a response should be marked as responded
CORE_CreateMacroVar CONVERSATION_AUTO_MARK_MESSAGE_RESPONDED conversation*auto*mark*message*responded

# Settings tag indicating this conversation should be automatically marked as complete after X ms without a response
CORE_CreateMacroVar CONVERSATION_AUTO_TIMEOUT conversation*auto*timeout

# Settings attribute indicating the auto timeout
CORE_CreateMacroVar CONVERSATION_AUTO_TIMEOUT_ATTR auto-timeout

# Settings attribute indicating when this conversation should be removed after completion, otherwise uses the lifecycle default
CORE_CreateMacroVar CONVERSATION_REMOVAL_ATTR removal-time

# Instance attribute automatically elaborated indicating the most recent activity on the conversation
CORE_CreateMacroVar CONVERSATION_LATEST_ACTIVITY_ATTR latest-activity

# Since NGS doesn't support creating typed objects from a binding (it's treated as a string in the TCL macro), introduce this mess :/
# TODO: I bet you could generize most of these rules and possibly add the others as part of declare-conversation
# TODO: Afraid to think about this, by make -by-channel an option as well? (probably not, by-user not by-channel will require look ups that this doesn't)
proc auto-classify-by-channel { conversationType messageCategory goalType { byMember "" } } {
    set scope [scope-name root "conversations*auto*classify*$messageCategory*into*$conversationType"]
    if { $byMember ne "" } {
        set scope "$scope*by*member"
    }
    # BEHOLD!  I have become all that I hate
    # (TCL can rot in hell, utter trash language)
    set memberLhsOutside ""
    set memberLhsInside ""
    set memberRhsInside ""

    if { $byMember ne "" } {
        set memberLhsOutside "
            [ngs-bind <message> member-id]
            [ngs-bind <conversation> member-id]
        "
    }
    sp "$scope*elaborate*decide*classification*choice*existing
        [ngs-match-goal-to-create-subgoal <s> $::D_CLASSIFY_MESSAGE <d> $goalType <pool>]
        [ngs-bind <d> message.parse channel conversations.conversation!$conversationType]
        [ngs-bind <channel> service-id server-id channel-id]
        [ngs-eq <parse> category $messageCategory]
        [ngs-is-tagged <conversation> $::CONVERSATION_ACTIVE]
        [ngs-bind <conversation> service-id server-id channel-id]
        $memberLhsOutside
    -->
        [ngs-create-goal-in-place <pool> $goalType $::NGS_GB_ACHIEVE <g> <d> { message <message> channel <channel> conversation <conversation> }]
        [ngs-assign-decision <g> $::D_CLASSIFY_MESSAGE $::NGS_NO]
    "

    if { $byMember ne "" } {
        set memberLhsOutside "[ngs-bind <message> member-id]"
        set memberLhsInside "[ngs-bind <c> member-id]"
    }
    sp "$scope*elaborate*decide*classification*choice*new
        [ngs-match-goal-to-create-subgoal <s> $::D_CLASSIFY_MESSAGE <d> $goalType <pool>]
        [ngs-bind <d> message.parse conversations channel]
        [ngs-eq <parse> category $messageCategory]
        [ngs-bind <channel> service-id server-id channel-id]
        $memberLhsOutside
        [ngs-not \
            [ngs-bind <conversations> conversation!$conversationType:<c>] \
            [ngs-is-tagged <c> $::CONVERSATION_ACTIVE] \
            [ngs-bind <c> service-id server-id channel-id] \
            $memberLhsInside \
        ]
    -->
        [ngs-create-goal-in-place <pool> $goalType $::NGS_GB_ACHIEVE <g> <d> { message <message> channel <channel> }]
        [ngs-assign-decision <g> $::D_CLASSIFY_MESSAGE $::NGS_NO]
    "

    sp "$scope*propose*decide*classification
        [ngs-match-to-make-choice <ss> $::D_CLASSIFY_MESSAGE <g> $::G_ACHIEVE_PROCESS_MESSAGE]
        [ngs-is-decision-choice <ss> <choice> $goalType]
    -->
        [ngs-make-choice-by-operator <ss> <choice>]
        [ngs-add-log-side-effect $scope $::NGS_INFO "Decided to classify"]
    "

    if { $byMember ne "" } {
        set memberLhsOutside "[ngs-bind <g> message.member-id]"
        set memberRhsInside "member-id <member-id>"
    }
    sp "$scope*propose*create*new*conversation
        [ngs-match-selected-goal <s> $goalType <g>]
        [ngs-nex <g> conversation]
        [ngs-bind <g> channel]
        [ngs-bind <channel> service-id server-id channel-id]
        $memberLhsOutside
    -->
        [ngs-create-typed-object-by-operator <s> <g> conversation $conversationType <c> "messages <m> responses <r> service-id <service-id> server-id <server-id> channel-id <channel-id> $memberRhsInside"]
        [ngs-add-primitive-side-effect $::NGS_SIDE_EFFECT_ADD <m> count 0]
    "

    sp "$scope*elaborate*decide*classification*accomplished
        [ngs-match-selected-goal <s> $goalType <g>]
        [ngs-bind <g> conversation!$conversationType]
    -->
        [ngs-tag-goal-achieved <g>]
    "
}