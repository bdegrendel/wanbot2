package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.ServerLocation
import com.degrendel.wanbot.common.inputs.*
import org.jsoar.kernel.io.quick.QMemory

class ServerImplementation(private val input: InputManager, override val memory: QMemory, location: ServerLocation)
  : Server, QMemoryDelegatable
{
  override val serviceId by QMemoryRODelegateProvider(location.serviceId)
  override val serverId by QMemoryRODelegateProvider(location.serverId)

  override val metadata: Metadata
    get() = TODO("not implemented")

  override var name by QMemoryRWDelegateProvider(serverId)
}
