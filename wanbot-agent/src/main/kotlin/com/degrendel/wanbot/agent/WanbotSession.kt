package com.degrendel.wanbot.agent

import com.degrendel.wanbot.agent.outputs.Halt
import com.degrendel.wanbot.agent.outputs.Recite
import com.degrendel.wanbot.agent.outputs.Request
import com.degrendel.wanbot.agent.outputs.Sleep
import com.degrendel.wanbot.common.*
import com.degrendel.wanbot.common.grammar.GrammarProvider
import com.degrendel.wanbot.common.output.OutputRequestHandler
import org.jsoar.kernel.io.beans.SoarBeanOutputContext
import org.jsoar.kernel.io.beans.SoarBeanOutputHandler
import java.lang.Exception
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

class WanbotSession(override val config: WanbotConfig, override val grammar: GrammarProvider) : Session
{
  companion object
  {
    val L by logger()
  }

  private val agent = WanbotSoarAgent(config)
  private val output = Output()
  private val textSpeakers = HashMap<String, TextOutputRelay>()

  override val input = SoarInputHandler(agent, config, grammar)

  override fun launch()
  {
    val withDebugger = config.c.getBoolean("agent.debugger")
    val autostart = config.c.getBoolean("agent.autostart")
    ServiceManager.start(this)
    Thread(output).start()
    agent.launch(withDebugger, autostart)
  }

  override fun terminate()
  {
    agent.terminate()
    output.running.set(false)
    ServiceManager.stop()
  }

  override fun registerTextOutputRelay(service: String, relay: TextOutputRelay)
  {
    if (textSpeakers.containsKey(service))
      L.error("Duplicate text output relay for {}: existing: {}, new: {}", service, textSpeakers[service], relay)
    else
      textSpeakers[service] = relay
  }

  override fun <T> registerOutputCommand(name: String, klass: Class<T>, handler: OutputRequestHandler<T>)
  {
    agent.manager.registerHandler(name, object : SoarBeanOutputHandler<T>()
    {
      override fun handleOutputCommand(context: SoarBeanOutputContext, command: T)
      {
        handler.handle(command)
        context.setStatusComplete()
      }
    }, klass)
  }

  inner class Output : Runnable
  {
    val running = AtomicBoolean(true)
    private val timeout = config.c.getLong("agent.output-poll-timeout-ms")

    override fun run()
    {
      while (running.get())
      {
        val request: Request = agent.output.poll(timeout, TimeUnit.MILLISECONDS) ?: continue

        // TODO: Should probably split these handlers out
        when (request)
        {
          is Sleep -> handleSleep(request)
          is Recite -> handleRecite(request)
          is Halt -> terminate()
        }
      }
    }

    private fun handleSleep(sleep: Sleep)
    {
      // TODO: ???
    }

    private fun handleRecite(recite: Recite)
    {
      // TODO: Probably want to assert server, channel, and message aren't ""
      val speaker = textSpeakers[recite.serviceId]
      if (speaker == null)
        L.error("Unknown or unhandled service {} for {}", recite.serviceId, recite)
      else if (recite.serverId.isBlank() || recite.channelId.isBlank())
        L.error("Recite request has blank server or channel!: {}", recite)
      else
      {
        if (recite.message.isBlank())
          L.warn("Agent produced empty message!: {}", recite)
        speaker.speak(recite, recite.message)
      }
    }
  }
}
