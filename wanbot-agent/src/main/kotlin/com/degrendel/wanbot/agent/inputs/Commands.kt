package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.MessageLocation
import java.time.Instant

sealed class Command(val timestamp: Long = Instant.now().toEpochMilli())

data class Ping(override val serviceId: String, override val serverId: String, override val channelId: String,
                override val memberId: String, val delay: Long = 0) : Command(), MessageLocation

data class Exec(override val serviceId: String, override val serverId: String, override val channelId: String,
                override val memberId: String, val command: String) : Command(), MessageLocation
