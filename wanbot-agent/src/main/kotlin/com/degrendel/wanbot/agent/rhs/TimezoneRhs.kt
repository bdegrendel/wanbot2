package com.degrendel.wanbot.agent.rhs

import org.jsoar.kernel.rhs.functions.AbstractRhsFunctionHandler
import org.jsoar.kernel.rhs.functions.RhsFunctionContext
import org.jsoar.kernel.symbols.Symbol
import java.time.Instant
import java.time.ZoneId
import java.time.temporal.ChronoField

class TimezoneRhs : AbstractRhsFunctionHandler("timezone", 1, 1)
{
  override fun execute(context: RhsFunctionContext, arguments: List<Symbol>): Symbol
  {
    val zone = ZoneId.of(arguments[0].asString().value)
    val offset = Instant.now().atZone(zone)
    val identifier = context.symbols.createIdentifier('t')
    val hour = context.symbols.createInteger(offset[ChronoField.CLOCK_HOUR_OF_AMPM].toLong())
    val minute = context.symbols.createInteger(offset[ChronoField.MINUTE_OF_HOUR].toLong())
    val second = context.symbols.createInteger(offset[ChronoField.SECOND_OF_MINUTE].toLong())
    val period = if (offset[ChronoField.AMPM_OF_DAY] == 0)
      context.symbols.createString("am")
    else
      context.symbols.createString("pm")
    val offsetSeconds = context.symbols.createInteger(offset.offset.totalSeconds.toLong())
    context.addWme(identifier, context.symbols.createString("hour"), hour)
    context.addWme(identifier, context.symbols.createString("minute"), minute)
    context.addWme(identifier, context.symbols.createString("second"), second)
    context.addWme(identifier, context.symbols.createString("period"), period)
    context.addWme(identifier, context.symbols.createString("offset"), offsetSeconds)
    return identifier
  }
}