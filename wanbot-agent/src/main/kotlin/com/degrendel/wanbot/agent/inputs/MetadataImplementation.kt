package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.inputs.InputManager
import com.degrendel.wanbot.common.inputs.Metadata
import org.jsoar.kernel.io.quick.QMemory

class MetadataImplementation(private val input: InputManager, private val memory: QMemory) : Metadata
{
  override fun get(key: String) = memory.getString(key)!!
  override fun set(key: String, value: String) = memory.setString(key, value)
  override fun contains(key: String) = memory.hasPath(key)
  override fun remove(key: String) = memory.remove(key)
}