package com.degrendel.wanbot.agent.rhs

import org.jsoar.kernel.rhs.functions.AbstractRhsFunctionHandler
import org.jsoar.kernel.rhs.functions.RhsFunctionContext
import org.jsoar.kernel.symbols.Symbol

class UppercaseRhs: AbstractRhsFunctionHandler("uppercase", 1, 1)
{
  override fun execute(context: RhsFunctionContext, args: List<Symbol>): Symbol
  {
    return context.symbols.createString(args[0].asString().value.toUpperCase())
  }
}
