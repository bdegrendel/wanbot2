package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.ServiceLocation
import com.degrendel.wanbot.common.inputs.InputManager
import com.degrendel.wanbot.common.inputs.Metadata
import com.degrendel.wanbot.common.inputs.Service
import org.jsoar.kernel.io.quick.QMemory

class ServiceImplementation(private val input: InputManager, override val memory: QMemory, location: ServiceLocation)
  : Service, QMemoryDelegatable
{
  override val serviceId: String by QMemoryRODelegateProvider(location.serviceId)
  override var name by QMemoryRWDelegateProvider(serviceId)

  override val metadata: Metadata
    get() = TODO("not implemented")
}
