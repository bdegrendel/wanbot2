package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.inputs.InputManager
import com.degrendel.wanbot.common.inputs.User
import org.jsoar.kernel.io.quick.QMemory

/**
 * Implementation of a global registered user.
 *
 * Soar input:
 *     <root>
 *       ^user-id: String
 *       ^name: String+
 *       ^default: String
 *
 *     <name>
 *       ^name: String
 *       ^lowercase: String
 *       ^default: Boolean
 */
class UserImplementation(private val input: InputManager, private val memory: QMemory, override val userId: String) : User
{
  init
  {
    memory.setString("user-id", userId)
  }

  override fun addAlias(name: String, default: Boolean)
  {
    memory.setString("name[$name]", name)
    if (default)
      memory.setString("default", name)
  }

  override fun removeAlias(name: String) = TODO("not implemented")
}
