package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.WanbotConfig
import com.degrendel.wanbot.common.inputs.Constants
import org.jsoar.kernel.io.quick.QMemory

class Capabilities(val memory: QMemory, config: WanbotConfig)
{
  init
  {
    val scope = config.c.subset("capabilities")
    scope.keys.forEach {
      if (scope.getBoolean(it))
        memory.setString("capabilities.$it", Constants.TRUE)
      else
        memory.setString("capabilities.$it", Constants.FALSE)
    }
  }
}