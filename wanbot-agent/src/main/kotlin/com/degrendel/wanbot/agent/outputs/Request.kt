package com.degrendel.wanbot.agent.outputs

import com.degrendel.wanbot.common.ChannelLocation
import com.degrendel.wanbot.common.output.OutputRequest
import java.time.Instant

sealed class Request(val timestamp: Instant = Instant.now())

// NOTE: To play nicely with NGSCommand handler, mark each field with @JvmField.  Otherwise you have to use properties,
// which are more challenging in Java land and who cares Soar would be 10x more fun if it wasn't such a pain getting
// data in and out of it.
class Sleep : Request(), OutputRequest

data class Recite(override var serviceId: String = "", override var serverId: String = "",
                  override var channelId: String = "", var message: String = "") : Request(), ChannelLocation

data class Halt(var source: String = "[not set]") : Request()
