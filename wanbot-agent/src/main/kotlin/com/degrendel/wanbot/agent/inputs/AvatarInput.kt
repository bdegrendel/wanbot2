package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.inputs.Avatar
import org.jsoar.kernel.io.quick.QMemory

class AvatarInput(override val memory: QMemory, avatar: Avatar): QMemoryDelegatable
{
  val userId by QMemoryRODelegateProvider(avatar.userId)
  val serviceId by QMemoryRODelegateProvider(avatar.serviceId)
  val memberId by QMemoryRODelegateProvider(avatar.memberId)
}
