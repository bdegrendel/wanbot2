package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.*
import com.degrendel.wanbot.common.inputs.Constants.FALSE
import com.degrendel.wanbot.common.inputs.Constants.TRUE
import org.jsoar.kernel.io.quick.QMemory
import org.jsoar.kernel.io.quick.QMemoryListener
import org.slf4j.LoggerFactory
import java.io.PrintStream
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

private val L = LoggerFactory.getLogger("com.degrendel.wanbot.agent.inputs.ObjectConverterKt")!!

fun <T : Any> addObjectToQMemory(memory: QMemory, attribute: String, data: T?, debug: Boolean = false)
{
  if (data == null)
    return
  val klass = data::class.java
  val interfaces = klass.interfaces
  // NOTE: Unclear why boxed primitives need javaObjectType, but String is A-OK with the standard class.java value
  if (klass == Int::class.javaObjectType)
  {
    if (debug)
      L.info("Adding integer (^{} {})", attribute, data)
    memory.setInteger(attribute, data as Int)
  }
  else if (klass == Long::class.javaObjectType)
  {
    if (debug)
      L.info("Adding long (^{} {})", attribute, data)
    memory.setInteger(attribute, data as Long)
  }
  else if (klass == String::class.java)
  {
    if (debug)
      L.info("Adding string (^{} |{}|)", attribute, data)
    memory.setString(attribute, data as String)
  }
  else if (klass == Double::class.javaObjectType)
  {
    if (debug)
      L.info("Adding double (^{} {})", attribute, data)
    memory.setDouble(attribute, data as Double)
  }
  else if (klass == Boolean::class.javaObjectType)
  {
    val value = if (data as Boolean) TRUE else FALSE
    if (debug)
      L.info("Adding boolean (^{} {})", attribute, value)
    memory.setString(attribute, value)
  }
  else if (List::class.java in interfaces)
  {
    val type = removePlural(attribute)
    @Suppress("UNCHECKED_CAST")
    val length = (data as List<Any>).size
    for (i in 0 until length)
    {
      val child = data[i]
      addObjectToQMemory(memory, "$type[$i]", child, debug)
    }
  }
  else if (klass.isEnum)
  {
    if (debug)
      L.info("Adding enum (^{} {}", attribute, data.toString())
    memory.setString(attribute, data.toString())
  }
  else
  {
    if (debug)
      L.info("Adding complex (^{} [object])", attribute)
    for (method in klass.methods)
    {
      // Getters only
      val name = method.name
      if (method.parameterCount != 0 || method.returnType == Void.TYPE || !name.startsWith("get")
          || name == "getClass")
        continue

      val type = removeUppercase(name.substring(3))
      if (type.isBlank())
        continue
      val value = method.invoke(data)
      addObjectToQMemory(memory.subMemory(attribute), type, value, debug)
    }
  }
}

private fun removePlural(name: String): String
{
  return when
  {
    name.endsWith("ies") -> name.substring(0, name.length - 3) + "y"
    name.endsWith("s") -> name.substring(0, name.length - 1)
    else -> name
  }
}

private fun removeUppercase(name: String): String
{
  if (name.isBlank()) return name
  val first = name[0].toLowerCase()
  val result = first + name.substring(1)
  return result.replace(Regex("[A-Z]")) { match: MatchResult -> "-" + match.value.toLowerCase() }
}

class OutputQMemory(private val output: PrintStream, private val align: String = "") : QMemory
{
  companion object
  {
    val WHITESPACE = Regex("\\s")
  }
  private val children = HashMap<String, OutputQMemory>()

  override fun setInteger(path: String, intValue: Int)
  {
    output.println("$align^$path $intValue")
  }

  override fun setInteger(path: String, longValue: Long)
  {
    output.println("$align^$path $longValue")
  }

  override fun setString(path: String, strValue: String)
  {
    if (strValue.contains(WHITESPACE))
      output.println("$align^$path |$strValue|")
    else
      output.println("$align^$path $strValue")
  }

  override fun subMemory(path: String): QMemory
  {
    return if (children.containsKey(path))
      children[path]!!
    else
    {
      output.println("$align^$path")
      val child = OutputQMemory(output, " ".repeat(align.length + path.length + 1))
      children[path] = child
      child
    }
  }

  override fun setDouble(path: String, doubleVal: Double)
  {
    output.println("$align^$path $doubleVal")
  }

  override fun getString(path: String?): String = TODO("not implemented")
  override fun getDouble(path: String): Double = TODO("not implemented")
  override fun getInteger(path: String): Long = TODO("not implemented")
  override fun clear(path: String) = TODO("not implemented")
  override fun hasPath(path: String): Boolean = TODO("not implemented")
  override fun remove(path: String) = TODO("not implemented")
  override fun addListener(listener: QMemoryListener) = TODO("not implemented")
  override fun getPaths(): MutableSet<String> = TODO("not implemented")
  override fun removeListener(listener: QMemoryListener) = TODO("not implemented")
}

interface QMemoryDelegatable
{
  val memory: QMemory
}

class QMemoryRODelegateProvider<T : Any>(private val value: T)
{
  operator fun provideDelegate(thisRef: QMemoryDelegatable, property: KProperty<*>):
      ReadOnlyProperty<QMemoryDelegatable, T>
  {
    val name = removeUppercase(property.name)

    when (value)
    {
      is String -> thisRef.memory.setString(name, value)
      is Int -> thisRef.memory.setInteger(name, value)
      is Long -> thisRef.memory.setInteger(name, value)
      is Boolean -> thisRef.memory.setString(name, if (value) TRUE else FALSE)
      is Double -> thisRef.memory.setDouble(name, value)
      is Enum<*> -> thisRef.memory.setString(name, value.name)
      is RoutingLocation -> addLocation(thisRef.memory, value)
      else -> throw IllegalArgumentException("Unhandled property delegate type of ${value::class} -- $value")
    }
    return QMemoryVal(value)
  }

  private fun addLocation(memory: QMemory, value: RoutingLocation)
  {
    if (value is ServiceLocation)
      memory.setString("service-id", value.serviceId)
    if (value is ServerLocation)
      memory.setString("server-id", value.serverId)
    if (value is ChannelLocation)
      memory.setString("channel-id", value.channelId)
    if (value is MemberLocation)
      memory.setString("member-id", value.memberId)
  }

  inner class QMemoryVal(private val value: T) : ReadOnlyProperty<QMemoryDelegatable, T>
  {
    override fun getValue(thisRef: QMemoryDelegatable, property: KProperty<*>): T = value
  }
}

class QMemoryRWDelegateProvider<T : Any>(private val value: T)
{
  operator fun provideDelegate(thisRef: QMemoryDelegatable, property: KProperty<*>):
      ReadWriteProperty<QMemoryDelegatable, T>
  {
    val name = removeUppercase(property.name)
    val result = QMemoryVar(name, value)
    result.setValue(thisRef, property, value)
    return result
  }

  inner class QMemoryVar(private val name: String, private var value: T) : ReadWriteProperty<QMemoryDelegatable, T>
  {
    override fun getValue(thisRef: QMemoryDelegatable, property: KProperty<*>): T = value

    override fun setValue(thisRef: QMemoryDelegatable, property: KProperty<*>, value: T)
    {
      when (value)
      {
        is String -> thisRef.memory.setString(name, value)
        is Int -> thisRef.memory.setInteger(name, value)
        is Long -> thisRef.memory.setInteger(name, value)
        is Boolean -> thisRef.memory.setString(name, if (value) TRUE else FALSE)
        is Double -> thisRef.memory.setDouble(name, value)
        is Enum<*> -> thisRef.memory.setString(name, value.name)
        else -> throw IllegalArgumentException("Unhandled property delegate type of ${value::class} -- $value")
      }
      this.value = value
    }
  }
}
