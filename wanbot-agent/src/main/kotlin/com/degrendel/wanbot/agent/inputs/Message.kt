package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.Error
import com.degrendel.wanbot.common.MessageLocation
import com.degrendel.wanbot.common.Result
import com.degrendel.wanbot.common.Success
import com.degrendel.wanbot.common.grammar.parses.Parse
import org.jsoar.kernel.io.quick.QMemory
import java.time.Instant

// TODO: Honestly, this could be replaced with a data class
class Message(override val memory: QMemory, internalMessageId: Int, location: MessageLocation, message: String,
              self: Boolean, parse: Result<Parse, String>, timestamp: Long, cycleCount: Long): QMemoryDelegatable
{
  val messageId by QMemoryRODelegateProvider(internalMessageId)
  val location by QMemoryRODelegateProvider(location)
  val self by QMemoryRODelegateProvider(self)
  val raw by QMemoryRODelegateProvider(message)
  val timestamp by QMemoryRODelegateProvider(timestamp)
  val cycleCount by QMemoryRODelegateProvider(cycleCount)

  init
  {
    when (parse)
    {
      is Error -> memory.setString("error", parse.value)
      is Success -> addObjectToQMemory(memory, "parse", parse.value)
    }
  }
}
