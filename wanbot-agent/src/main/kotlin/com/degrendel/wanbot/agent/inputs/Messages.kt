package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.MessageLocation
import com.degrendel.wanbot.common.WanbotConfig
import com.degrendel.wanbot.common.grammar.GrammarProvider
import com.degrendel.wanbot.common.inputs.InputManager
import com.degrendel.wanbot.common.logger
import org.jsoar.kernel.io.quick.QMemory
import java.time.Instant

class Messages(private val parent: InputManager, private val memory: QMemory, config: WanbotConfig, private val grammar: GrammarProvider)
{
  companion object
  {
    val L by logger()
  }

  private val maxMessages = config.c.getInt("input.max-messages")
  private val maxMessageLength = config.c.getInt("input.max-message-length")
  private var nextMessageId = 0
  private val messages = ArrayList<Message>()

  fun addMessage(location: MessageLocation, message: String, self: Boolean, timestamp: Instant, cycleCount: Long)
  {
    if (message.length > maxMessageLength)
    {
      L.info("Ignoring message of length {}", message.length)
      return
    }

    val msg = Message(memory.subMemory("message[$nextMessageId]"), nextMessageId, location, message,
        self, grammar.parse(message), timestamp.toEpochMilli(), cycleCount)
    messages.add(msg)
    L.info("Adding message with id {} / {}", msg.messageId, nextMessageId)
    nextMessageId++

    while (messages.size > maxMessages)
    {
      val remove = messages.removeAt(0)
      memory.remove("message[${remove.messageId}")
      L.info("Removing message with id {}", remove.messageId)
    }
  }
}
