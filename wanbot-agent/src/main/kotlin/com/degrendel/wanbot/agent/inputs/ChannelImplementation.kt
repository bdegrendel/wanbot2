package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.ChannelLocation
import com.degrendel.wanbot.common.inputs.*
import org.jsoar.kernel.io.quick.QMemory

class ChannelImplementation(private val input: InputManager, override val memory: QMemory, location: ChannelLocation)
  : Channel, QMemoryDelegatable
{
  override val serviceId by QMemoryRODelegateProvider(location.serviceId)
  override val serverId by QMemoryRODelegateProvider(location.serverId)
  override val channelId by QMemoryRODelegateProvider(location.channelId)

  override var name by QMemoryRWDelegateProvider(channelId)

  override val metadata = MetadataImplementation(input, memory.subMemory("metadata"))
}
