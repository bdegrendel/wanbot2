package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.WanbotConfig
import com.degrendel.wanbot.common.inputs.GenericInput
import com.degrendel.wanbot.common.inputs.InputManager
import org.jsoar.kernel.io.quick.QMemory

class GenericInputs(private val parent: InputManager, private val memory: QMemory, config: WanbotConfig)
{
  private val maxEntries = config.c.getInt("input.max-generic-entries")
  private val entries = ArrayList<GenericInput>()

  fun addEntry(entry: GenericInput)
  {
    addObjectToQMemory(memory, "input[${entry.id}]", entry)
    entries.add(entry)

    while (entries.size > maxEntries)
    {
      memory.remove("input[${entries[0].id}]")
      entries.removeAt(0)
    }
  }
}
