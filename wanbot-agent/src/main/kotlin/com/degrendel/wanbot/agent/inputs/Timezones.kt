package com.degrendel.wanbot.agent.inputs

import org.jsoar.kernel.io.quick.QMemory
import java.time.Instant
import java.time.ZoneId

class Timezones(val memory: QMemory)
{
  fun addTimezone(zone: ZoneId, human: String, names: Array<String>)
  {
    val identifier = zone.id
    val child = memory.subMemory("timezone[$identifier]")
    val offset = Instant.now().atZone(zone)
    child.setInteger("offset", offset.offset.totalSeconds.toLong() * 1000)
    child.setString("identifier", identifier)
    child.setString("human", human)
    names.forEachIndexed { i, name -> child.setString("name[$i]", name) }
  }
}
