package com.degrendel.wanbot.agent

import com.degrendel.wanbot.agent.inputs.Capabilities
import com.degrendel.wanbot.agent.inputs.Command
import com.degrendel.wanbot.agent.inputs.Timezones
import com.degrendel.wanbot.agent.inputs.addObjectToQMemory
import com.degrendel.wanbot.agent.outputs.Recite
import com.degrendel.wanbot.agent.outputs.Request
import com.degrendel.wanbot.agent.outputs.Sleep
import com.degrendel.wanbot.agent.rhs.LowercaseRhs
import com.degrendel.wanbot.agent.rhs.NextRequestIdRhs
import com.degrendel.wanbot.agent.rhs.TimezoneRhs
import com.degrendel.wanbot.agent.rhs.UppercaseRhs
import com.degrendel.wanbot.common.WanbotConfig
import com.degrendel.wanbot.common.logger
import org.jsoar.kernel.SoarException
import org.jsoar.kernel.events.InputEvent
import org.jsoar.kernel.io.CycleCountInput
import org.jsoar.kernel.io.beans.SoarBeanOutputContext
import org.jsoar.kernel.io.beans.SoarBeanOutputHandler
import org.jsoar.kernel.io.beans.SoarBeanOutputManager
import org.jsoar.kernel.io.quick.DefaultQMemory
import org.jsoar.kernel.io.quick.QMemory
import org.jsoar.kernel.io.quick.SoarQMemoryAdapter
import org.jsoar.kernel.memory.Wmes
import org.jsoar.kernel.symbols.Identifier
import org.jsoar.runtime.ThreadedAgent
import org.jsoar.util.commands.SoarCommands
import org.jsoar.util.events.SoarEvent
import java.io.Writer
import java.time.Instant
import java.time.ZoneId
import java.time.temporal.ChronoField
import java.time.temporal.TemporalField
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong
import kotlin.system.exitProcess

class WanbotSoarAgent(config: WanbotConfig)
{
  companion object
  {
    val L by logger()
  }

  val agent = ThreadedAgent.create("Wanbot")!!
  val shouldSleep = AtomicBoolean(false)

  // Input
  private val inputTimeout = config.c.getLong("agent.input-poll-timeout-ms")
  val input = LinkedBlockingQueue<Command>()
  private val qMemory: QMemory = DefaultQMemory.create()
  private val qMemoryAdapter: SoarQMemoryAdapter
  private val timezones: Timezones

  private var cycleCount = AtomicLong(0)

  private val capabilities = Capabilities(qMemory, config)

  // Output
  val output = LinkedBlockingQueue<Request>()
  val manager = SoarBeanOutputManager(agent.events)

  // Output command handler
  private var nextCommandId = 0
  private val commands = HashMap<String, Identifier>()
  private val commandsToIdentify = HashSet<String>()
  private val commandMatcher = Wmes.matcher(agent.symbols).attr("done").createPredicate()

  init
  {
    agent.rhsFunctions.registerHandler(UppercaseRhs())
    agent.rhsFunctions.registerHandler(LowercaseRhs())
    agent.rhsFunctions.registerHandler(NextRequestIdRhs())
    agent.rhsFunctions.registerHandler(TimezoneRhs())

    try
    {
      SoarCommands.source(agent.interpreter, javaClass.getResource("/soar/load.soar"))
    }
    catch (e: SoarException)
    {
      // TODO: There should be a better way to terminate than this
      L.error("Unable to source the agent!", e)
      exitProcess(-1)
    }
    qMemoryAdapter = SoarQMemoryAdapter.attach(agent.agent, qMemory)

    timezones = Timezones(qMemory.subMemory("timezones"))
    qMemory.setString("clock.timezone", ZoneId.systemDefault().id)

    manager.registerHandler("recite", object : SoarBeanOutputHandler<Recite>()
    {
      override fun handleOutputCommand(context: SoarBeanOutputContext, request: Recite)
      {
        output.put(request)
        context.setStatusComplete()
      }
    }, Recite::class.java)

    manager.registerHandler("sleep", object : SoarBeanOutputHandler<Sleep>()
    {
      override fun handleOutputCommand(context: SoarBeanOutputContext, request: Sleep)
      {
        shouldSleep.set(true)
        context.setStatusComplete()
      }
    }, Sleep::class.java)

    qMemory.setString("clock.timezone", ZoneId.systemDefault().id)

    agent.events.addListener(InputEvent::class.java) {
      try
      {
        if (commandsToIdentify.isNotEmpty())
        {
          for (path in commandsToIdentify)
            commands[path] = qMemoryAdapter.getValue(path).asIdentifier()
          commandsToIdentify.clear()
        }
        val command: Command? = if (shouldSleep.get())
        {
          shouldSleep.set(false)

          // Basically iterate over known commands, if they have a ^done attribute remove them
          // Yes, this is kinda ugly and falls into the giant category of "things Soar should probably be able to do
          // out of the box without any legwork"
          val toDrop = HashSet<String>()
          for ((path, identifier) in commands)
          {
            if (Wmes.find(identifier.wmes, commandMatcher) != null)
            {
              qMemory.remove(path)
              toDrop.add(path)
            }
          }
          commands.keys.removeAll(toDrop)

          input.poll(inputTimeout, TimeUnit.MILLISECONDS)
        }
        else
          input.poll()

        if (command != null)
        {
          val path = "commands.${command::class.simpleName!!.toLowerCase()}[$nextCommandId]"
          nextCommandId++
          addObjectToQMemory(qMemory, path, command, true)
          commandsToIdentify.add(path)
        }
      }
      catch (_: InterruptedException)
      {
        L.info("Input event handler interrupted!")
      }
      qMemory.setInteger("cycle-count", cycleCount.incrementAndGet())
      qMemory.setInteger("clock.timestamp", Instant.now().toEpochMilli())
    }

    agent.printer.addPersistentWriter(object : Writer()
    {
      val buffer = StringBuffer()
      val match = Regex("\\[(DEBUG|ERROR|FATAL|INFO|TRACE|WARN)")

      override fun write(chars: CharArray, offset: Int, length: Int)
      {
        buffer.append(chars, offset, length)
        var newline = buffer.indexOf("\n")
        while (newline >= 0)
        {
          val line = buffer.substring(0, newline)
          buffer.delete(0, newline + 1)

          if (!line.isBlank() && !match.containsMatchIn(line))
            println(line)

          newline = buffer.indexOf("\n")
        }
      }

      override fun flush()
      {
      }

      override fun close()
      {
      }
    })
  }

  fun getCycleCount(): Long
  {
    return cycleCount.get()
  }

  fun openDebugger()
  {
    agent.openDebugger()
    // TODO: This seems like it should work, but doesn't :/
    // Debugger overwrites the watch preferences (which gets noisy), and there doesn't seem to be a great way
    // to force it back to something sane
    //agent.execute(Callable<Unit> {
    //  agent.interpreter.eval("watch --decisions 0")
    //}, null)
  }

  fun launch(withDebugger: Boolean, autostart: Boolean)
  {
    L.info("STARTING AGENT: DEBUGGER? {}, AUTOSTART? {}", withDebugger, autostart)
    if (withDebugger)
      openDebugger()

    if (autostart)
      agent.runForever()
  }

  fun terminate()
  {
    L.info("TERMINATING AGENT")
    agent.execute(Callable<Unit> {
      Thread.currentThread().interrupt()
    }, null)
    agent.dispose()
  }
}
