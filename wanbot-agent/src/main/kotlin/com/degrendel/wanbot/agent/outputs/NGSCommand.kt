package com.degrendel.wanbot.agent.outputs

import com.degrendel.wanbot.agent.WanbotSoarAgent
import com.degrendel.wanbot.common.logger
import org.jsoar.kernel.io.InputWmes
import org.jsoar.kernel.io.commands.OutputCommandHandler
import org.jsoar.kernel.memory.Wme
import org.jsoar.kernel.memory.Wmes
import org.jsoar.kernel.symbols.Identifier
import java.time.Instant
import kotlin.reflect.KClass

class NGSCommand(private val agent: WanbotSoarAgent) : OutputCommandHandler
{
  companion object
  {
    val L by logger()
  }

  // NOTE: This call (.sealedSubclasses) is kinda expensive, so we will definitely need to cache it
  private val requests = Request::class.sealedSubclasses

  private val statusMatcher = Wmes.matcher(agent.agent.symbols).attr("status").createPredicate()
  private val myTypeMatcher = Wmes.matcher(agent.agent.symbols).attr("my-type").createPredicate()

  override fun onCommandRemoved(command: String, identifier: Identifier)
  {
  }

  private fun findMatchingClass(myType: String): KClass<out Request>?
  {
    // NOTE: Sealed classes must be in the same file as the parent, so safe to assume no duplicate simpleNames?
    for (request in requests)
    {
      if (request.simpleName == myType)
        return request
    }
    L.warn("Didn't find a Request with a simpleName matching {}", myType)
    return null
  }

  // TODO: This is somewhat slow FYI (~4ms, which feels expensive for a simple operation)
  // Also it would be easy and probably crazy fast to do in the dumb C style in C :/
  // Probably the regex's fault
  //private fun rename(soar: String): String =
  //    soar.replace(Regex("""[\-*](?<char>\w)""")) { match: MatchResult ->
  //      match.groups["char"]!!.value.toUpperCase()
  //    }

  // TODO: This is faster, but appears to still be a bit sluggish
  // Irritating that it's clear how to do this correctly and very quick in C, but not in Kotlin
  private fun rename(soar: String, capFirst: Boolean = false): String
  {
    val builder = StringBuilder()
    var capNext = capFirst
    soar.forEach {
      run {
        if (it == '_' || it == '*')
          capNext = true
        else if (capNext)
        {
          capNext = false
          builder.append(it.toUpperCase())
        }
        else
          builder.append(it)
      }
    }
    return builder.toString()
  }

  override fun onCommandAdded(command: String, identifier: Identifier)
  {
    L.trace("WANBOT IS STARTING ON A COMMAND @ {}", Instant.now())
    // Get ^my-type, aborting if obviously incorrect (log occurs in helper)
    val statusWme: Wme? = Wmes.find(identifier.wmes, statusMatcher)
    if (statusWme != null)
    {
      L.warn("onCommandAdded fired for an existing command $command!?")
      return
    }
    val myTypeWme: Wme? = Wmes.find(identifier.wmes, myTypeMatcher)
    if (myTypeWme == null)
    {
      L.warn("Got command $command but no ^my-type?")
      return
    }
    val myType = myTypeWme.value.asString().value
    // NOTE: we were renaming myType from like*this to LikeThis, but it's expensive and not actually necessary
    // val klassName = rename(myType, true)
    // Find a class extending Request or abort (log message occurs in helper)
    val klass = findMatchingClass(myType) ?: return

    // Create new instance of class corresponding to output class
    val request = createAndPopulateClass(klass, myType, myType, identifier)
    L.trace("WANBOT IS REQUESTING {}", request)
    when (request)
    {
      is Sleep -> agent.shouldSleep.set(true)
      else -> agent.output.add(request)
    }
    // TODO: I guess it'd be trivial to generate the 'proper' NGS done status
    InputWmes.add(agent.agent.inputOutput, identifier, "status", "complete")
    L.trace("WANBOT IS DONE @ {}", Instant.now())
  }

  private fun <T : Any> createAndPopulateClass(klass: KClass<T>, soarType: String, klassName: String,
                                               identifier: Identifier): T
  {
    // FYI: klass.createInstance() is extremely slow compared to klass.java.newInstance()
    // What gives?  Internally, KClass and Class are largely the same thing according to the debugger
    val target = klass.java.getDeclaredConstructor().newInstance()
    // NOTE: doing the member lookups via Kotlin reflection requires getting all KProperty members , which turns
    // out to be fairly slow.
    // Iterate over each child WME
    for (wme in identifier.wmes)
    {
      val attrSoar = wme.attribute.asString().value
      // Type data or NGS junk (__*)? Skip it
      if (attrSoar == "my-type" || attrSoar == "type" || attrSoar.startsWith("__"))
        continue
      // Rename from interesting-value to interestingValue
      val attribute = rename(attrSoar)
      val field = try
      {
        target::class.java.getField(attribute)
      }
      catch (e: NoSuchFieldException)
      {
        L.warn("Found ^{} in {}, but didn't find field {} in {}", attrSoar, soarType, attribute, klassName)
        continue
      }
      // TODO: Should confirm that the field is mutable and non-static
      val fieldType = field.genericType
      when (fieldType)
      {
        String::class.java -> field.set(target, wme.value.asString().value)
        Int::class.java -> field.set(target, wme.value.asInteger().value.toInt())
        Long::class.java -> field.set(target, wme.value.asInteger().value)
        Double::class.java -> field.set(target, wme.value.asDouble().value)
        else ->
        {
          L.info("Falling through on ^{} {}", wme.attribute, wme.value)
          val childIdentifier: Identifier? = wme.value.asIdentifier();
          if (childIdentifier == null)
          {
            L.warn("Unhandled non identifier ^{} {} expected {}", wme.attribute, wme.value, fieldType)
          }
          else
          {
            // TODO: Does this work at all?  That KClass feels sketchy as all hell
            val child = createAndPopulateClass(field.type.kotlin, attrSoar, fieldType.typeName, childIdentifier)
            field.set(target, child)
          }
        }
      }
    }
    return target
  }
}
