package com.degrendel.wanbot.agent.rhs

import org.jsoar.kernel.rhs.functions.AbstractRhsFunctionHandler
import org.jsoar.kernel.rhs.functions.RhsFunctionContext
import org.jsoar.kernel.symbols.Symbol
import java.util.concurrent.atomic.AtomicLong

class NextRequestIdRhs: AbstractRhsFunctionHandler("next-request-id", 0, 0)
{
  private val nextId = AtomicLong(0)

  override fun execute(context: RhsFunctionContext, args: List<Symbol>): Symbol
  {
    return context.symbols.createInteger(nextId.getAndIncrement())
  }
}
