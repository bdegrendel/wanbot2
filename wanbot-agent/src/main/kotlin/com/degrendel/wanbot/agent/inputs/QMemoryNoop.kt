package com.degrendel.wanbot.agent.inputs

import org.jsoar.kernel.io.quick.QMemory
import org.jsoar.kernel.io.quick.QMemoryListener

class QMemoryNoop : QMemory
{
  override fun clear(path: String)
  {
  }

  override fun hasPath(path: String): Boolean = TODO("not implemented")

  override fun remove(path: String)
  {
  }

  override fun addListener(listener: QMemoryListener)
  {
  }

  override fun setInteger(path: String, intVal: Int)
  {
  }

  override fun setInteger(path: String, longVal: Long)
  {
  }

  override fun getPaths(): MutableSet<String> = HashSet()

  override fun getInteger(path: String): Long = TODO("not implemented")

  override fun removeListener(listener: QMemoryListener)
  {
  }

  override fun setString(path: String, strVal: String)
  {
  }

  override fun getString(path: String?): String = TODO("not implemented")

  override fun subMemory(prefix: String): QMemory = this

  override fun setDouble(path: String, doubleVal: Double)
  {
  }

  override fun getDouble(path: String): Double = TODO("not implemented")
}