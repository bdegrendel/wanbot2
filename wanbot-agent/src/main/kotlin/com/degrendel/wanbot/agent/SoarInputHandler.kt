package com.degrendel.wanbot.agent

import com.degrendel.wanbot.agent.inputs.*
import com.degrendel.wanbot.agent.outputs.Halt
import com.degrendel.wanbot.agent.outputs.Recite
import com.degrendel.wanbot.common.*
import com.degrendel.wanbot.common.grammar.GrammarProvider
import com.degrendel.wanbot.common.inputs.*
import org.jsoar.kernel.SoarException
import org.jsoar.kernel.io.quick.DefaultQMemory
import org.jsoar.kernel.io.quick.QMemory
import org.jsoar.kernel.io.quick.SoarQMemoryAdapter
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.nio.charset.Charset
import java.time.Instant

class SoarInputHandler(private val agent: WanbotSoarAgent, config: WanbotConfig,
                       private val grammar: GrammarProvider) : InputManager
{
  companion object
  {
    val L by logger()
    const val S_TO_MS: Long = 1000
    val COMMAND_REGEX = Regex("!(?<command>\\w+)\\s*(?<payload>.*)")
  }

  private val users = HashMap<String, UserImplementation>()
  private val services = HashMap<String, ServiceImplementation>()
  private val servers = HashMap<String, ServerImplementation>()
  private val channels = HashMap<String, ChannelImplementation>()
  private val members = HashMap<String, MemberImplementation>()
  private val avatars = HashMap<String, AvatarInput>()
  private val qMemory: QMemory = DefaultQMemory.create()
  private val messages = Messages(this, qMemory.subMemory("messages"), config, grammar)
  private val entries = GenericInputs(this, qMemory.subMemory("inputs"), config)

  init
  {
    SoarQMemoryAdapter.attach(agent.agent.agent, qMemory)
  }

  // TODO: Should these synchronize on qMemory?
  @Synchronized
  override fun getService(service: ServiceLocation): Service
  {
    val id = toServiceId(service)
    if (id !in services)
    {
      val serviceImpl = ServiceImplementation(this, qMemory.subMemory("services.service[$id]"), service)
      services[id] = serviceImpl
    }
    return services[id]!!
  }

  @Synchronized
  override fun dropService(service: ServiceLocation) = TODO("not implemented")

  private fun toServiceId(loc: ServiceLocation) = loc.serviceId

  @Synchronized
  override fun getUser(userId: String): User
  {
    if (userId !in users)
    {
      val user = UserImplementation(this, qMemory.subMemory("users.user[$userId]"), userId)
      users[userId] = user
    }
    return users[userId]!!
  }

  @Synchronized
  override fun dropUser(userId: String) = TODO("not implemented")

  @Synchronized
  override fun addAvatar(avatar: Avatar)
  {
    val id = toAvatarId(avatar)
    if (id in avatars)
      L.warn("ATTEMPTED TO ADD DUPLICATE AVATAR $avatar")
    else
      avatars[id] = AvatarInput(qMemory.subMemory("avatars.avatar[$id]"), avatar)
  }

  @Synchronized
  override fun dropAvatar(avatar: Avatar) = TODO("not implemented")

  private fun toAvatarId(avatar: Avatar) = "${avatar.userId}::${avatar.serviceId}::${avatar.memberId}"

  @Synchronized
  override fun getServer(server: ServerLocation): Server
  {
    val id = toServerId(server)
    if (id !in servers)
    {
      val serverImpl = ServerImplementation(this, qMemory.subMemory("servers.server[$id]"), server)
      servers[id] = serverImpl
    }
    return servers[id]!!
  }

  @Synchronized
  override fun dropServer(server: ServerLocation) = TODO("not implemented")

  private fun toServerId(loc: ServerLocation) = "${toServiceId(loc)}::${loc.serverId}"

  @Synchronized
  override fun getChannel(channel: ChannelLocation): Channel
  {
    val id = toChannelId(channel)
    if (id !in channels)
    {
      val channelImpl = ChannelImplementation(this, qMemory.subMemory("channels.channel[$id]"), channel)
      channels[id] = channelImpl
    }
    return channels[id]!!
  }

  @Synchronized
  override fun dropChannel(channel: ChannelLocation) = TODO("not implemented")

  private fun toChannelId(loc: ChannelLocation) = "${toServerId(loc)}::${loc.channelId}"

  @Synchronized
  override fun getMember(member: MemberLocation): Member
  {
    val id = toMemberId(member)
    if (id !in members)
    {
      val memberImpl = MemberImplementation(this, qMemory.subMemory("members.member[$id]"), member)
      members[id] = memberImpl
    }
    return members[id]!!
  }

  @Synchronized
  override fun dropMember(member: MemberLocation) = TODO("not implemented")

  private fun toMemberId(loc: MemberLocation) = "${toServerId(loc)}::${loc.memberId}"

  private fun createRecite(location: ChannelLocation, message: String): Recite
  {
    return Recite(serviceId = location.serviceId, serverId = location.serverId, channelId = location.channelId,
        message = message)
  }

  @Synchronized
  override fun addMessage(location: MessageLocation, message: String, self: Boolean, timestamp: Instant, allowed: Set<InputManager.MagicCommands>)
  {
    // TODO: Logic here is still kinda funky
    if (!self && handleMagicCommand(location, message, allowed))
      return
    val cleaned = if (message.startsWith("!")) message.substring(1) else message
    messages.addMessage(location, cleaned, self, timestamp, agent.getCycleCount())
  }

  @Synchronized
  override fun addGenericInput(input: GenericInput) = entries.addEntry(input)

  private fun handleMagicCommand(location: MessageLocation, message: String, allowed: Set<InputManager.MagicCommands>): Boolean
  {
    val commandMatch = COMMAND_REGEX.matchEntire(message) ?: return false
    val name = commandMatch.groups["command"]!!.value
    val payload = commandMatch.groups["payload"]
    val command = try
    {
      InputManager.MagicCommands.valueOf(name.toUpperCase())
    }
    catch (_: IllegalArgumentException)
    {
      return false
    }

    if (command !in allowed)
    {
      agent.output.put(createRecite(location, "(Nuh uh uh, you didn't say the magic word, jackass)"))
      return true
    }

    when (command)
    {
      InputManager.MagicCommands.DEBUGGER -> agent.openDebugger()
      InputManager.MagicCommands.HALT -> agent.output.put(Halt("service:${location.serviceId}::member:${location.memberId}"))
      InputManager.MagicCommands.PING ->
      {
        try
        {
          val delay = if (payload != null && payload.value.isNotBlank()) payload.value.toLong() * S_TO_MS else 0
          agent.input.put(Ping(location.serviceId, location.serverId, location.channelId, location.memberId, delay))
        }
        catch (_: NumberFormatException)
        {
          agent.output.put(createRecite(location, "(Ping takes an integer argument, jackass)"))
        }
      }
      InputManager.MagicCommands.EXEC ->
      {
        if (payload == null || payload.value.isBlank())
          agent.output.put(createRecite(location, "(Exec needs a command to run)"))
        else
        {
          try
          {
            agent.agent.interpreter.eval(payload.value)
          }
          catch (e: SoarException)
          {
            agent.output.put(createRecite(location, "SoarException!: ${e.localizedMessage}"))
          }
        }
      }
      InputManager.MagicCommands.GRAMMAR ->
      {
        val outbound = if (payload == null || payload.value.isBlank())
          "(PROTIP: parses as ^parse.category FUCK-YOU, look I don't even need to check)"
        else
        {
          when (val parse = grammar.parse(payload.value))
          {
            is Success ->
            {
              val stream = ByteArrayOutputStream()
              addObjectToQMemory(OutputQMemory(PrintStream(stream)), "parse", parse.value, false)
              stream.toString(Charset.defaultCharset().name())
            }
            is Error -> parse.value
          }
        }
        agent.output.put(createRecite(location, outbound))
      }
    }
    return true
  }

  override fun transaction(func: (InputManager) -> Unit)
  {
    synchronized(qMemory, fun()
    {
      func(this)
    })
  }
}
