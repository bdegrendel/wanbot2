package com.degrendel.wanbot.agent.inputs

import com.degrendel.wanbot.common.MemberLocation
import com.degrendel.wanbot.common.inputs.InputManager
import com.degrendel.wanbot.common.inputs.Member
import org.jsoar.kernel.io.quick.QMemory

class MemberImplementation(private val input: InputManager, override val memory: QMemory, location: MemberLocation)
  : Member, QMemoryDelegatable
{
  override var onAllChannels by QMemoryRWDelegateProvider(false)

  override fun addChannel(channelId: String) = memory.setString("channel-id[$channelId]", channelId)
  override fun dropChannel(channelId: String) = memory.remove("channel-id[$channelId]")

  override val serviceId by QMemoryRODelegateProvider(location.serviceId)
  override val serverId by QMemoryRODelegateProvider(location.serverId)
  override val memberId by QMemoryRODelegateProvider(location.memberId)

  override var state by QMemoryRWDelegateProvider(Member.State.ONLINE)

  override var name by QMemoryRWDelegateProvider(memberId)
}
