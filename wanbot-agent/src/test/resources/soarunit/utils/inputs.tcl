proc _expect_input_context { context } {
    upvar 2 input_context input_context
    if { ![info exists input_context] } {
        error "[lindex [info level -1] 0] expects to be in LHS context $context, but none is set (missing \[fake-input...\]?)"
    } elseif { ($input_context ne $context) } {
        error "[lindex [info level -1] 0] expects state $context, but we're in $input_context: [_get_stacktrace]"
    }
}

proc _get_stacktrace { } {
    set ret ""
    set distanceToTop [info level]
    for {set i 1} {$i < $distanceToTop} {incr i} {
        set callerlevel [expr {$distanceToTop - $i}]
        set ret "$ret\nCALLER $callerlevel: [info level $callerlevel]"
    }
    return $ret
}

proc enable-capability { name { state "" } } {
    if { $state eq "" } {
        set state $::NGS_YES
    }
    sp "enable*capability*$name
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> capabilities]
    -->
        [ngs-create-attribute <capabilities> $name $state]"
}

proc fake-standard-input { number_users block } {
    # (screams in TCL)
    namespace eval fake {}
    set ::fake::block $block
    set ::fake::number_users $number_users
    if { $number_users > 1 } {
        set ::fake::private $::NGS_NO
    } else {
        set ::fake::private $::NGS_YES
    }
    fake-input {
        # Hey, why is it ::fake::i versus just i?
        # BECAUSE TCL IS A COMPLETE TRASH LANGUAGE
        # fake-user ... {
        #    $i <-- CAN'T READ i WITHOUT BEING DOUBLE QUOTES
        # }
        # IF IT HAD LEGIT LAMBDAS AND WHATNOT, WOULD BE A NONISSUE
        for { set ::fake::i 1 } { $::fake::i <= $::fake::number_users } { incr ::fake::i } {
            fake-user soarunit "" {
                fake-avatar service-1 member-$::fake::i
            }
        }
        fake-service "" {
            fake-server "" {
                fake-channel [dict create private $::fake::private] $::fake::block
                for { set i 1 } { $i <= $::fake::number_users } { incr i } {
                    fake-member local-soarunit-$i
                }
            }
        }
    }
    namespace delete fake
}

proc fake-input { block } {
    upvar input_context parent_input_context
    if { [info exists parent_input_context] } {
        error "[lindex [info level 0] 0] expects not be in a fake input context, but we're in $parent_input_context"
    }

    namespace eval input {
        set user_id 0
        set service_id 0
        set server_id 0
        set channel_id 0
        set avatar_id 0
        set member_id 0
        set message_id 0
        set command_id 0
    }

    set input_context root

    eval $block

    namespace delete input
}

proc _metadata_to_rhs { metadata } {
    if { [dict size $metadata] != 0 } {
        set rhs "(<metadata>"
        dict for {key value} $metadata {
            set rhs "$rhs ^$key $value"
        }
        set rhs "$rhs)"
    } else {
        set rhs ""
    }
    return $rhs
}


proc fake-user { names metadata block } {
    _expect_input_context root
    incr ::input::user_id
    set user_name "user-$::input::user_id"
    set ::input::user_name $user_name
    set input_context user

    set names_rhs ""

    if { [llength $names] < 1 } {
        error "fake-user expects a list of at least one name, got [llength $names]"
    }

    set names_rhs "^default |[lindex names 0]| "
    foreach name $names {
        set names_rhs "$names_rhs ^name |$name|"
    }

    sp "input*fake*$::input::user_name
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> users]
    -->
        (<users> ^user <user>)
        (<user> $names_rhs ^user-id user-$::input::user_id ^metadata <metadata>)
        [_metadata_to_rhs $metadata]"

    eval $block
}

proc fake-avatar { service_id member_id } {
    _expect_input_context user
    incr ::input::avatar_id

    sp "input*fake*user-$::input::user_id*avatar-$::input::avatar_id
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> avatars]
    -->
        (<avatars> ^avatar <avatar>)
        (<avatar> ^user-id user-$::input::user_id ^service-id $service_id ^member-id $member_id)
    "
}

proc fake-service { metadata block } {
    _expect_input_context root
    incr ::input::service_id
    set service_name "service-$::input::service_id"
    set ::input::service_name $service_name
    set input_context service

    sp "input*fake*$::input::service_name
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> services]
    -->
        (<services> ^service <service>)
        (<service> ^metadata <metadata> ^service-id service-$::input::service_id ^name |$::input::service_name|)
        [_metadata_to_rhs $metadata]"
    
    eval $block
}

proc fake-server { metadata block } {
    _expect_input_context service
    incr ::input::server_id
    set server_name "server-$::input::server_id"
    set ::input::server_name $server_name
    set input_context server

    sp "input*fake*$::input::server_name
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> servers]
    -->
        (<servers> ^server <server>)
        (<server> ^metadata <metadata> ^service-id service-$::input::service_id ^server-id server-$::input::server_id ^name |$::input::server_name|)
        [_metadata_to_rhs $metadata]"
    
    eval $block
}

proc fake-channel { metadata block } {
    _expect_input_context server
    incr ::input::channel_id
    set channel_name "channel-$::input::channel_id"
    set ::input::channel_name $channel_name
    set input_context channel

    sp "input*fake*$::input::channel_name
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> channels]
    -->
        (<channels> ^channel <channel>)
        (<channel> ^metadata <metadata> ^service-id service-$::input::service_id ^server-id server-$::input::server_id ^channel-id channel-$::input::channel_id ^name |$::input::channel_name|)
        [_metadata_to_rhs $metadata]"
    
    eval $block
}

proc fake-member { local_name { all_channels "" } { member_state "ONLINE" } } {
    _expect_input_context server
    incr ::input::member_id

    if { $all_channels eq "" } {
        set all_channels $::NGS_YES
    }

    sp "input*fake*member-$::input::member_id
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> members]
    -->
        (<members> ^member <member>)
        (<member> ^service-id service-$::input::service_id ^server-id server-$::input::server_id ^member-id member-$::input::member_id ^local-name |$local_name| ^on-all-channels $all_channels ^state $member_state)"
}

proc fake-parsed-message { cycle member_id raw parse timestamp { self "" } } {
    _expect_input_context channel
    incr ::input::message_id

    if { $self eq "" } {
        set self $::NGS_NO
    }

    sp "input*fake*parsed*message-$::input::message_id*at*$cycle
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> messages]
        [ngs-stable-gte <input> cycle-count $cycle]
    -->
        (<messages> ^message <message>)
        (<message> ^service-id service-$::input::service_id ^server-id server-$::input::server_id ^channel-id channel-$::input::channel_id ^member-id $member_id ^message-id $::input::message_id ^raw |$raw| ^timestamp $timestamp ^parse <parse> ^self $self)
        $parse"
}

proc fake-unparsed-message { cycle member_id raw error timestamp { self "" } } {
    _expect_input_context channel
    incr ::input::message_id

    if { $self eq "" } {
        set self $::NGS_NO
    }

    sp "input*fake*unparsed*message-$::input::message_id*at*$cycle
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> messages]
        [ngs-stable-gte <input> cycle-count $cycle]
    -->
        (<messages> ^message <message>)
        (<message> ^service-id service-$::input::service_id ^server-id server-$::input::server_id ^channel-id channel-$::input::channel_id ^member-id $member_id ^message-id $::input::message_id ^raw |$raw| ^timestamp $timestamp ^error |$error| ^self $self)"
}

proc fake-ping { cycle member_id timestamp { delay 0 } } {
    _expect_input_context channel
    incr ::input::command_id

    sp "input*fake*ping*command-$::input::command_id*at*$cycle
        [ngs-match-top-state <s> "" <input>]
        [ngs-bind <input> commands]
        [ngs-stable-gte <input> cycle-count $cycle]
    -->
        (<commands> ^ping <ping>)
        (<ping> ^service-id service-$::input::service_id ^server-id server-$::input::server_id ^channel-id channel-$::input::channel_id ^member-id $member_id ^delay $delay ^timestamp $timestamp)
    "
}