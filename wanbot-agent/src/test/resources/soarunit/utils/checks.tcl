proc match-recite { message { output "<output>" } { binding "<recite>" } { service_id "service-1" } { server_id "server-1" } { channel_id "channel-1" } } {
    return "($output ^recite $binding)
            ($binding ^service-id $service_id ^server-id $server_id ^channel-id $channel_id ^message |$message|)"
}