package com.degrendel.wanbot.technobabble

import com.degrendel.wanbot.common.Session
import com.degrendel.wanbot.common.WanbotPlugin
import com.degrendel.wanbot.common.logger
import com.degrendel.wanbot.common.output.OutputRequestHandler
import org.jsoup.Jsoup
import kotlin.concurrent.thread

class TechnobabblePlugin : WanbotPlugin, OutputRequestHandler<TechnobabbleRequest>
{
  companion object
  {
    val L by logger()
  }

  lateinit var session: Session

  override fun name(): String = "Technobabble"

  override fun start(session: Session)
  {
    this.session = session
    session.registerOutputCommand("technobabble", TechnobabbleRequest::class.java, this)
  }

  override fun stop()
  {
    // TODO: Anything need to be done here?
  }

  override fun handle(request: TechnobabbleRequest)
  {
    thread(start = true) {
      try
      {
        val document = Jsoup.connect("http://www.technobabble.biz/").get()
        val problem = document.selectFirst("#problem")
        val solution = document.selectFirst("#solution")
        session.input.addGenericInput(TechnobabbleResponse(request.id, problem.text(), solution.text()))
      }
      catch (e: Exception)
      {
        L.error("Unable to get a pair for request ${request.id}", e)
      }
    }
  }
}
