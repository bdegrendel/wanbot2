package com.degrendel.wanbot.technobabble

import com.degrendel.wanbot.common.output.OutputRequest

data class TechnobabbleRequest(val id: Long): OutputRequest
