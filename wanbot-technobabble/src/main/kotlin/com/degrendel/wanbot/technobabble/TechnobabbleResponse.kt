package com.degrendel.wanbot.technobabble

import com.degrendel.wanbot.common.inputs.GenericInput

data class TechnobabbleResponse(override val id: Long, val problem: String, val solution: String): GenericInput
