package com.degrendel.wanbot.common.grammar

import com.degrendel.wanbot.common.Result
import com.degrendel.wanbot.common.grammar.parses.Parse

interface GrammarProvider
{
    fun parse(raw: String): Result<Parse, String>
}
