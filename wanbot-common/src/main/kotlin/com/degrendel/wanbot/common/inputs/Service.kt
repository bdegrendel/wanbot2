package com.degrendel.wanbot.common.inputs

import com.degrendel.wanbot.common.ServiceLocation

interface Service: ServiceLocation
{
  var name: String
  val metadata: Metadata
}
