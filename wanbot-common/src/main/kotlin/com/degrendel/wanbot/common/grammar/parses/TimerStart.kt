package com.degrendel.wanbot.common.grammar.parses

class TimerStart(override val prompt: Boolean, val `when`: TimeQuantity, val target: Entity): Parse
{
  override val category = "timer-start"
}

enum class TimeUnit(val label: String)
{
  SECONDS("seconds"),
  MINUTES("minutes"),
  HOURS("hours");

  override fun toString() = label
}

enum class TimePeriod(val label: String)
{
  AM("am"),
  PM("pm");

  override fun toString() = label
}

sealed class TimeQuantity
data class RelativeTime(val unit: String, val quantity: Long): TimeQuantity()
data class AbsoluteTime(val hour: Int, val minute: Int, val period: TimePeriod?, val offset: Long?, val timezone: String?): TimeQuantity()
