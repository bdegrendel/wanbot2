package com.degrendel.wanbot.common.grammar.parses

class Prompt: Parse
{
    override val prompt = true
    override val category = "prompt"

    override fun equals(other: Any?): Boolean
    {
        return (other is Prompt)
    }

    override fun hashCode(): Int
    {
        var result = prompt.hashCode()
        result = 31 * result + category.hashCode()
        return result
    }
}
