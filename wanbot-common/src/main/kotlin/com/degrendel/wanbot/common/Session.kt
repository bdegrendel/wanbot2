package com.degrendel.wanbot.common

import com.degrendel.wanbot.common.grammar.GrammarProvider
import com.degrendel.wanbot.common.inputs.InputManager
import com.degrendel.wanbot.common.output.OutputRequestHandler

interface Session
{
  val input: InputManager
  val config : WanbotConfig
  val grammar: GrammarProvider

  fun launch()

  fun terminate()

  fun registerTextOutputRelay(service: String, relay: TextOutputRelay)

  fun <T> registerOutputCommand(name: String, klass: Class<T>, handler: OutputRequestHandler<T>)
}
