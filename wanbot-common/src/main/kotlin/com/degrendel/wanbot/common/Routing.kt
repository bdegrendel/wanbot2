package com.degrendel.wanbot.common

// NOTE: Any additional interfaces might need handling inside ObjectConverter
interface RoutingLocation

interface ServiceLocation : RoutingLocation
{
  val serviceId: String
}

interface ServerLocation : ServiceLocation, RoutingLocation
{
  val serverId: String
}

interface ChannelLocation : ServerLocation, RoutingLocation
{
  val channelId: String
}

interface MemberLocation : ServerLocation, RoutingLocation
{
  val memberId: String
}

interface MessageLocation : ChannelLocation, MemberLocation, RoutingLocation
