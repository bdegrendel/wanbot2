package com.degrendel.wanbot.common

interface TextOutputRelay
{
  fun speak(location: ChannelLocation, message: String)
}
