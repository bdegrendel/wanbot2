package com.degrendel.wanbot.common.inputs

import com.degrendel.wanbot.common.*
import java.time.Instant

interface InputManager
{
  enum class MagicCommands(private val label: String)
  {
    HALT("halt"),
    PING("ping"),
    EXEC("exec"),
    DEBUGGER("debugger"),
    GRAMMAR("grammar");

    override fun toString(): String = label
  }

  fun transaction(func: (InputManager) -> Unit)

  fun getService(service: ServiceLocation): Service
  fun dropService(service: ServiceLocation)

  fun getServer(server: ServerLocation): Server
  fun dropServer(server: ServerLocation)

  fun getChannel(channel: ChannelLocation): Channel
  fun dropChannel(channel: ChannelLocation)

  fun getMember(member: MemberLocation): Member
  fun dropMember(member: MemberLocation)

  fun getUser(userId: String): User
  fun dropUser(userId: String)

  fun addAvatar(avatar: Avatar)
  fun dropAvatar(avatar: Avatar)

  fun addMessage(location: MessageLocation, message: String, self: Boolean, timestamp: Instant = Instant.now(), allowed: Set<MagicCommands> = HashSet())

  fun addGenericInput(input: GenericInput)
}
