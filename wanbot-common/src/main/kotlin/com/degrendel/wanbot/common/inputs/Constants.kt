package com.degrendel.wanbot.common.inputs

object Constants
{
  const val TRUE = "*YES*"
  const val FALSE = "*NO*"
}