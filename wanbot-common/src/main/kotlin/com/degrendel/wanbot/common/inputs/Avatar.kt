package com.degrendel.wanbot.common.inputs

data class Avatar(val userId: String, val serviceId: String, val memberId: String)
