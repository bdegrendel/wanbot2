package com.degrendel.wanbot.common

import org.apache.commons.configuration2.CombinedConfiguration
import org.apache.commons.configuration2.builder.fluent.Configurations
import org.apache.commons.configuration2.tree.OverrideCombiner
import java.io.File
import java.net.URL


/**
 * Handles configuration management across.
 *
 * Priorities:
 * * /configs/default.properties (bundled)
 * * /configs/profile.properties (bundled, optional)
 * * $env:WANBOT_CONFIG (external, optional)
 * * external (external, optional)
 *
 * Later configs shadow earlier settings.
 */
class WanbotConfig(profile: String = "", override: String = "")
{
  companion object
  {
    val L by logger()

    const val DEFAULT_PATH = "/configs/default.properties"
    const val ENV_VARIABLE = "WANBOT_CONFIG"
  }

  val c = CombinedConfiguration(OverrideCombiner())

  init
  {
    // Oh why oh why isn't this a static utils class Apache?
    val configs = Configurations()
    loadConfig(configs, "override", override, false)

    val env = System.getenv(ENV_VARIABLE) ?: ""
    loadConfig(configs, "\$env:$ENV_VARIABLE", env, false)

    loadConfig(configs, "profile", "/configs/$profile.properties", true)

    if (!loadConfig(configs, "default", DEFAULT_PATH, true))
    {
      throw IllegalStateException("Unable to load the default properties from $DEFAULT_PATH")
    }
  }

  private fun loadConfig(configs: Configurations, source: String, path: String, bundled: Boolean): Boolean
  {
    if (path.isBlank())
    {
      L.info("SKIPPING {} DUE TO BLANK PATH", source)
      return false
    }
    if (bundled)
    {
      val url: URL? = javaClass.getResource(path)
      if (url == null)
      {
        L.info("PROPERTIES {} [BUNDLED] NOT FOUND, SKIPPING {}", path, source)
        return false
      }
      L.info("LOADING {} PROPERTIES FROM {} [BUNDLED]", source, path)
      c.addConfiguration(configs.properties(url))
      return true
    }
    else
    {
      val file = File(path)
      L.info("LOADING {} PROPERTIES FROM {} [EXTERNAL]", source, path)
      c.addConfiguration(configs.properties(file))
      return true
    }
  }
}