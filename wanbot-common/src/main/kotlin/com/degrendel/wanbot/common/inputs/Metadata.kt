package com.degrendel.wanbot.common.inputs

interface Metadata
{
  operator fun get(key: String): String
  operator fun set(key: String, value: String)
  operator fun contains(key: String): Boolean

  fun remove(key: String)
}
