package com.degrendel.wanbot.common.inputs

import com.degrendel.wanbot.common.ChannelLocation

interface Channel: ChannelLocation
{
  val metadata: Metadata
  var name: String
}
