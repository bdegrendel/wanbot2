package com.degrendel.wanbot.common.grammar.parses

data class BadBot(override val prompt: Boolean) : Parse
{
  override val category = "bad-bot"
}

data class FChain(override val prompt: Boolean) : Parse
{
  override val category = "f"
}

data class GoodBot(override val prompt: Boolean) : Parse
{
  override val category = "good-bot"
}

data class RIP(override val prompt: Boolean, val target: Entity) : Parse
{
  override val category = "rip"
}

data class WhereIs(override val prompt: Boolean, val target: Entity) : Parse
{
  override val category = "where-is"
}

data class WTF(override val prompt: Boolean) : Parse
{
  override val category = "wtf"
}

data class Wut(override val prompt: Boolean) : Parse
{
  override val category = "wut"
}
