package com.degrendel.wanbot.common.inputs

import com.degrendel.wanbot.common.ServerLocation

interface Server: ServerLocation
{
  val metadata: Metadata
  var name: String
}
