package com.degrendel.wanbot.common.inputs

interface User
{
  fun addAlias(name: String, default: Boolean = false)
  fun removeAlias(name: String)

  val userId: String
}
