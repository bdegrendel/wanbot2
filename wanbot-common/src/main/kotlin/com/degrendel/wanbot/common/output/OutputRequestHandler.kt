package com.degrendel.wanbot.common.output

interface OutputRequestHandler<T>
{
  fun handle(request: T)
}
