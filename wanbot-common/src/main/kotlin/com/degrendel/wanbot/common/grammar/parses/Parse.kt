package com.degrendel.wanbot.common.grammar.parses

interface Parse
{
  val prompt: Boolean
  val category: String
}
