package com.degrendel.wanbot.common.inputs

import com.degrendel.wanbot.common.MemberLocation

interface Member: MemberLocation
{
  enum class State
  {
    ONLINE, AWAY, BUSY
  }

  var name: String
  var state: State
  var onAllChannels: Boolean

  fun addChannel(channelId: String)
  fun dropChannel(channelId: String)
}
