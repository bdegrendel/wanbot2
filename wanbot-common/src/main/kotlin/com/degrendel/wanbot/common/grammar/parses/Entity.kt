package com.degrendel.wanbot.common.grammar.parses

class Entity(entityRaw: String, possessorRaw: String?)
{
  val entity = when
  {
    entityRaw.isBlank() -> null
    else -> entityRaw
  }

  val possessor = when
  {
    possessorRaw == null -> null
    possessorRaw.isBlank() -> null
    possessorRaw.endsWith("'s") -> possessorRaw.removeSuffix("'s")
    possessorRaw.endsWith("s'") -> possessorRaw.removeSuffix("s'")
    else -> possessorRaw
  }
}
