package com.degrendel.wanbot.common

import java.util.*
import kotlin.reflect.KClass

object ServiceManager
{
  private val L by logger()

  private val plugins = HashMap<KClass<out WanbotPlugin>, WanbotPlugin>()

  init
  {
    val serviceLoader = ServiceLoader.load(WanbotPlugin::class.java)

    for (plugin in serviceLoader)
    {
      L.info("FOUND PLUGIN {} :: {}", plugin, plugin.name())
      plugins[plugin::class] = plugin
    }
  }

  fun start(session: Session)
  {
    for (plugin in plugins.values)
    {
      L.info("STARTING PLUGIN {} :: {}", plugin, plugin.name())
      plugin.start(session)
    }
  }

  fun stop()
  {
    for (plugin in plugins.values)
    {
      L.info("STOPPING PLUGIN [] :: []", plugin, plugin.name())
      plugin.stop()
    }
  }

  operator fun get(plugin: KClass<out WanbotPlugin>): WanbotPlugin = plugins[plugin]!!
}
