package com.degrendel.wanbot.common

interface WanbotPlugin
{
  fun name(): String

  fun start(session: Session)
  fun stop()
}
