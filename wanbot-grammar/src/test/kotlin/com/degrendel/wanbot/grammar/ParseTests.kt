package com.degrendel.wanbot.grammar

import com.degrendel.wanbot.common.Success
import com.degrendel.wanbot.common.grammar.parses.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class ParseTests
{
  private val grammar = AntlrGrammarProvider()

  @ParameterizedTest
  @ValueSource(strings = ["wat", "what", "whaat", "wut", "wuut"])
  fun wut(input: String)
  {
    val success = Success<Parse, String>(Wut(false))
    assertEquals(success, grammar.parse(input), input)
  }

  @ParameterizedTest
  @ValueSource(strings = ["hey wanbot", "hey wanbot?", "hey wanbot,", "hey wanbot!", "hey wanbot.", "wanbot", "wanbot?", "hey"])
  fun prompt(input: String)
  {
    val success = Success<Parse, String>(Prompt())
    assertEquals(success, grammar.parse(input), input)
  }

  @ParameterizedTest
  @ValueSource(strings = ["f", "fff", "f ff", "fff ff f", "fFfFFffffff f?", "F!", "f."])
  fun fChain(input: String)
  {
    val success = Success<Parse, String>(FChain(false))
    assertEquals(success, grammar.parse(input), input)
  }

  @ParameterizedTest
  @ValueSource(strings = ["badbot", "bad bot", "bad fucking bot"])
  fun badBot(input: String)
  {
    val success = Success<Parse, String>(BadBot(false))
    assertEquals(success, grammar.parse(input), input)
  }

  @ParameterizedTest
  @ValueSource(strings = ["goodbot", "good bot", "good fucking bot"])
  fun goodBot(input: String)
  {
    val success = Success<Parse, String>(GoodBot(false))
    assertEquals(success, grammar.parse(input), input)
  }
}
