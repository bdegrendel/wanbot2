package com.degrendel.wanbot.grammar

import com.degrendel.wanbot.common.Error
import com.degrendel.wanbot.common.Result
import com.degrendel.wanbot.common.Success
import com.degrendel.wanbot.common.grammar.GrammarProvider
import com.degrendel.wanbot.common.grammar.parses.*
import org.antlr.v4.runtime.*
import org.antlr.v4.runtime.misc.ParseCancellationException
import org.antlr.v4.runtime.tree.TerminalNode
import java.time.Instant
import java.time.ZoneId


class AntlrGrammarProvider : SpeechBaseVisitor<Parse>(), GrammarProvider
{
  companion object
  {
    val POSSESSIVE = Regex("'s|s'")

    fun List<TerminalNode>.combineToSpacedString(): String
    {
      return joinToString(separator = " ") { it.text }
    }
  }

  override fun parse(raw: String): Result<Parse, String>
  {
    return try
    {
      // Thanks for https://stackoverflow.com/questions/18132078/handling-errors-in-antlr4 for this error handling
      // solution.  Also thanks to Kotlin for object singletons.
      val charStream = CharStreams.fromString(raw)
      val lower = CaseChangingCharStream(charStream, false)

      val lexer = SpeechLexer(lower)
      lexer.removeErrorListeners()
      lexer.addErrorListener(ThrowingErrorListener)

      val tokens = CommonTokenStream(lexer)

      val parser = SpeechParser(tokens)
      parser.removeErrorListeners()
      parser.addErrorListener(ThrowingErrorListener)

      Success(visit(parser.speech()))
    }
    // NOTE: Kotlin doesn't have Java style multi exception types, so this mess:
    catch (e: Exception)
    {
      Error("Unable to parse: ${e.message}")
    }
  }

  object ThrowingErrorListener : BaseErrorListener()
  {
    override fun syntaxError(recognizer: Recognizer<*, *>, offendingSymbol: Any?, line: Int, charPositionInLine: Int,
                             msg: String, e: RecognitionException?)
    {
      throw ParseCancellationException("line $line:$charPositionInLine $msg")
    }
  }

  class EmptyInputException(message: String) : Exception(message)
  class InternalParseError(message: String) : Exception(message)

  override fun visitSpeech(ctx: SpeechParser.SpeechContext): Parse
  {
    val prompt = ctx.prompt() != null
    return when (val message = ctx.message())
    {
      (null) ->
      {
        if (!prompt)
          throw EmptyInputException("No prompt and no message")
        Prompt()
      }
      else -> MessageVisitor(prompt).visitMessage(message)

    }
  }

  class MessageVisitor(private val prompt: Boolean) : SpeechBaseVisitor<Parse>()
  {
    override fun visitMessage(ctx: SpeechParser.MessageContext): Parse
    {
      return when
      {
        ctx.badBot() != null -> BadBot(prompt)
        ctx.f() != null -> FChain(prompt)
        ctx.goodBot() != null -> GoodBot(prompt)
        ctx.rip() != null -> RIP(prompt, EntityVisitor.visitEntity(ctx.rip().entity()))
        ctx.whereIs() != null -> WhereIs(prompt, EntityVisitor.visitEntity(ctx.whereIs().entity()))
        ctx.wtf() != null -> WTF(prompt)
        ctx.wut() != null -> Wut(prompt)
        ctx.timerStart() != null -> TimerStart(prompt,
            TimeQuantityVisitor.visitTimeQuantity(ctx.timerStart().timeQuantity()),
            EntityVisitor.visitEntity(ctx.timerStart().userFutureStatus().entity()))
        else -> throw InternalParseError("Message context, but subcontext isn't handled!")
      }
    }
  }

  object TimeQuantityVisitor : SpeechBaseVisitor<TimeQuantity>()
  {
    override fun visitTimeQuantity(ctx: SpeechParser.TimeQuantityContext): TimeQuantity
    {
      return when
      {
        ctx.absoluteTime() != null -> visitAbsoluteTime(ctx.absoluteTime())
        ctx.relativeTime() != null -> visitRelativeTime(ctx.relativeTime())
        else -> throw InternalParseError("TimeQuantity context is unhandled!: $ctx")
      }
    }

    override fun visitAbsoluteTime(ctx: SpeechParser.AbsoluteTimeContext): TimeQuantity
    {
      val hour = ctx.quantity(0).text.toInt()
      val minute = if (ctx.quantity().size > 1)
        ctx.quantity(1).text.toInt()
      else 0
      val period = if (ctx.timePeriod() != null)
      {
        when (ctx.timePeriod().text)
        {
          "am" -> TimePeriod.AM
          "pm" -> TimePeriod.PM
          else -> throw InternalParseError("Unhandled time period ${ctx.timePeriod().text}")
        }
      }
      else
        null
      if (ctx.timezone() != null)
      {
        val (zone, timezone) = when (ctx.timezone().text)
        {
          "est", "eastern" -> Pair(ZoneId.of("America/New_York"), "EST")
          "cst", "central" -> Pair(ZoneId.of("America/Chicago"), "CST")
          else -> throw InternalParseError("Unhandled timezone ${ctx.timezone().text}")
        }
        val offsetMilliseconds = zone.rules.getOffset(Instant.now()).totalSeconds.toLong() * 1000
        return AbsoluteTime(hour, minute, period, offsetMilliseconds, timezone)
      }
      return AbsoluteTime(hour, minute, period, null, null)
    }

    override fun visitRelativeTime(ctx: SpeechParser.RelativeTimeContext): TimeQuantity
    {
      return super.visitRelativeTime(ctx)
    }
  }

  object EntityVisitor : SpeechBaseVisitor<Entity>()
  {
    override fun visitEntity(ctx: SpeechParser.EntityContext): Entity
    {
      for (it in ctx.ENTITY().iterator().withIndex())
      {
        val text = it.value.text.toLowerCase()
        if (text == "my" || text == "your" || text == "his" || text == "hers" || text == "its"
            || POSSESSIVE.containsMatchIn(text))
        {
          return Entity(ctx.ENTITY().subList(it.index + 1, ctx.ENTITY().size).combineToSpacedString(),
              ctx.ENTITY().subList(0, it.index + 1).combineToSpacedString())
        }
      }
      return Entity(ctx.ENTITY().combineToSpacedString(), null)
    }
  }
}
