grammar Speech;

speech : prompt? message? END_OF_SENTENCE? EOF;

prompt : ('hey' | me | 'hey' me ) ','?;

message : badBot | computerIssue | f | goodBot | rip | whereIs | wtf | wut | timerStart;

f : F+;

wut : WHAT | WUT;

wtf : 'wtf' | WHAT theFuck;

rip : ('rip' | 'rest' 'in' intensifier* 'peace') entity?;

whereIs : 'where' theFuck? ('is' | 'are') entity;

theFuck : 'the' intensifier* 'fuck';

badBot : ('badbot' | 'bad' intensifier* me);
goodBot : ('goodbot' | 'good' intensifier* me);

timerStart : timeQuantity ','? userFutureStatus | userFutureStatus timeQuantity;

timeQuantity : 'in' relativeTime | 'at' absoluteTime;

absoluteTime : quantity (':' quantity?)? timePeriod? timezone?;
relativeTime : quantity timeUnit;

timePeriod : 'am' | 'pm';

timezone : 'est' | 'eastern' | 'cst' | 'central';

userFutureStatus: entity 'will' 'be' ('ready' | 'online' | 'back');

computerIssue : WHAT 'is' 'wrong' intensifier* 'with' 'my'? ENTITY+;

intensifier : 'everliving' | 'goddamn' | 'fucking';
me: 'bot' | 'wanbot';

timeUnit : 'minute' | 'minutes' | 'second' | 'seconds' | 'hour' | 'hours';

entity : ENTITY+;

quantity : NUMERIC;

END_OF_SENTENCE : [.?!];

F : 'f'+;

WHAT : 'w' 'h'? 'a'+ 't';
WUT : 'w' 'u'+ 't';

NUMERIC : [0-9]+;

ENTITY : ENTITY_START ENTITY_BODY*;
fragment ENTITY_START : [a-z0-9_\-];
fragment ENTITY_BODY : ENTITY_START | '\'';
WHITESPACE : [ \t\r\n]+ -> skip;
