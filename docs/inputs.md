# Inputs

## Root

    state <s> ^io.input-link <input>
    <input> ^services <services>
            ^servers <servers>
            ^channels <channels>
            ^members <members>
            ^users <users>
            ^avatars <avatars>
            ^messages <messages>
            ^commands <commands>
            ^clock <clock>
    <services> ^service <service>: Service
    <servers> ^server <server>: Server
    <channels> ^channel <channel>: Channel
    <users> ^user <user>: User
    <avatars> ^avatar <avatar>: Avatar
    <members> ^member <member>: Member
    <messages> ^message <message>: Message
    <commands> ^<command-type> <command>
    <clock>:
        timestamp: Long -- absolute timestamp at the beginning of the current cycle
        timezone: Str -- timezone identifier indicating where wanbot is

## Command

See /docs/commands.md

## Metadata

    <meta> ^<key>: String <value>: String

## User

    <user> ^user-id: String [user-id]
           ^online: Boolean [elaborated]
           ^seen: Boolean [elaborated]
           ^name: String+
           ^default: String
           ^lowercase <lowercase> [elaborated]
    <lowercase> ^name: String

## Avatars

    <avatar> ^user-id: String [user-id]
             ^member-id: String [member-id]
             ^service-id: String [service-id]

## Service

    <service> ^service-id: String [service-id]
              ^name: String
              ^metadata: Metadata

## Server

    <server> ^server-id: String [server-id]
             ^service-id: String [service-id]
             ^name: String
             ^metadata <metadata>: Metadata

## Channel

    <channel> ^channel-id: String [channel-id]
              ^service-id: String [service-id]
              ^server-id: String [server-id]
              ^name: String
              ^metadata: Metadata

## Member

    <member> ^member-id: String [member-id]
             ^server-id: String [server-id]
             ^service-id: String [service-id]
             ^local-name: String
             ^on-all-channels: Boolean
             ^state: Enum (ONLINE, AWAY, OFFLINE)
             ^metadata: Metadata

## Message

    <message> ^message-id: Int
              ^member-id: String [member-id]
              ^channel-id: String [channel-id]
              ^server-id: String [server-id]
              ^service-id: Service [service-id]
              ^self: Bool [whether this from Wanbot]
              ^raw: String
              ^timestamp: Long [epoch second in ms]
              ^error: String?
              ^parse <parse>: Parse?

## Parse

See /docs/parses.md
