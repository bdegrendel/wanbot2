# Behavior Examples

## F with Prompt

* hey wanbot, f --> (^category f ^prompt True)

* i-support link into channel
* AchieveHandleFMessageGoal on message
* i-support previous messages queries
* add must respond tag to message
* create CreateFResponse goal
* Mark message as tagged responded f
* MaintainChannelResponses goal selects f

## Multiple Fs

* F --> (^category F)
* F --> (^category F)

* i-support link each message into the channel
* AchieveHandleFMessageGoal on each message
* i-support previous messages queries
* create CreateFResponse goal from pair of messages found tag
* create CreateFResponse goal
* mark messages as tagged responded f
* MaintainChannelResponses goal selects f
