# Parses

## Common

    ^parse
      ^category: String [identifier]
      ^prompt: Boolean

## Prompt

    ^category prompt

## F

    ^category f

## RIP

    ^category rip
    ^target
        ^entity: String? [entity name]

## WTF

    ^category wtf

## Where is USER

    ^category where-is-user
    ^target
        ^entity: String [user name]

## Timer start query

    ^category timer-start
    ^target
        ^entity: String [user name]
    ^time
        ^type: Enum (relative|absolute)
        ^quantity: Long? [original amount user entered]
        ^units: Enum? (second|minutes|hours)
        ^hour: Long? [entered hours for absolute]
        ^minute: Long? [entered minutes for absolute]
        ^timezone: String? [entered timezone for absolute]
