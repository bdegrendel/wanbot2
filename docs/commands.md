# Commands (input commands)

    ^(type)
      ^done: Boolean [when agent has handled this command - technically any value will trigger its removal]

## Ping

    ^ping
      ^delay: Int [how long to wait before responding, in seconds]
