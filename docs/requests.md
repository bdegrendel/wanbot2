# Requests (output commands)

## Sleep

    ^command
      ^my-type Sleep
      ^cycle: Int [Soar cycle number when this was generated]

## Recite

    ^command
      ^my-type Recite
      ^text: String [what to output]
      ^service: String [service-id]
      ^server: String [server-id]
      ^channel: String [channel-id]
