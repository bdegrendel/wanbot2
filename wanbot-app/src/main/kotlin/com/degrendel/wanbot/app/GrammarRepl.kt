package com.degrendel.wanbot.app

import com.degrendel.wanbot.agent.inputs.OutputQMemory
import com.degrendel.wanbot.agent.inputs.addObjectToQMemory
import com.degrendel.wanbot.common.Error
import com.degrendel.wanbot.common.Success
import com.degrendel.wanbot.common.grammar.GrammarProvider
import com.degrendel.wanbot.common.logger
import java.util.*

class GrammarRepl
{
  companion object
  {
    val L by logger()
  }

  fun launch(grammar: GrammarProvider)
  {
    val scanner = Scanner(System.`in`)
    L.info("GRAMMAR REPL READY")
    while (true)
    {
      val line = scanner.nextLine()
      if (line == "!halt") break
      System.out.println("> $line")
      when (val result = grammar.parse(line))
      {
        is Success -> addObjectToQMemory(OutputQMemory(System.out), "parse", result.value, false)
        is Error -> println(result.value)
      }
    }
    L.info("HALTING GRAMMAR REPL")
  }
}
