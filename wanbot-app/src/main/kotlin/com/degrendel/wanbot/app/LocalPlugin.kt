package com.degrendel.wanbot.app

import com.degrendel.wanbot.common.*
import com.degrendel.wanbot.common.inputs.Constants
import com.degrendel.wanbot.common.inputs.InputManager
import com.degrendel.wanbot.common.inputs.Member
import java.time.Instant
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class LocalPlugin : WanbotPlugin, TextOutputRelay
{
  companion object
  {
    val L by logger()
    const val SERVICE_ID = "localhost"
    const val SERVER_ID = "local"
    const val CHANNEL_ID = "stdin"
    const val MEMBER_ID = "direct"
  }

  private val magic = InputManager.MagicCommands.values().toSet()

  override fun name() = "Local Interface"

  override fun speak(location: ChannelLocation, message: String)
  {
    if (location.serverId != SERVER_ID || location.channelId != CHANNEL_ID)
      L.warn("Unexpected server ({} <> {}) or channel ({} <> {})", location.serverId, SERVER_ID, location.channelId, CHANNEL_ID)
    else
    {
      L.info("WANBOT > {}", message)
      thread?.let {
        it.input.addMessage(it.location, message, true, Instant.now(), magic)
      }
    }
  }

  inner class Input(val input: InputManager) : Runnable, ServiceLocation
  {
    override val serviceId = SERVICE_ID
    val location = object : MessageLocation
    {
      override val serviceId = SERVICE_ID
      override val serverId = SERVER_ID
      override val channelId = CHANNEL_ID
      override val memberId = MEMBER_ID
    }

    override fun run()
    {
      val service = input.getService(this)
      service.name = "Local"
      val channel = input.getChannel(location)
      channel.metadata["private"] = Constants.TRUE
      input.getServer(location)
      val member = input.getMember(location)
      member.name = "ADMINATOR"
      member.state = Member.State.ONLINE

      while (running.get())
      {
        val line = scanner.nextLine()
        L.info("Line is {}", line)
        input.addMessage(location, line, false, Instant.now(), magic)
      }
    }

    val running = AtomicBoolean(true)
    val scanner = Scanner(System.`in`)
  }

  override fun start(session: Session)
  {
    session.registerTextOutputRelay(SERVICE_ID, this)
    thread = Input(session.input)
    Thread(thread).start()
  }

  override fun stop()
  {
    thread?.running?.set(false)
    thread?.scanner?.close()
  }

  private var thread: Input? = null
}
