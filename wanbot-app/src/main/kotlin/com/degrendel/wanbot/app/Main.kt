package com.degrendel.wanbot.app

import com.degrendel.wanbot.agent.WanbotSession
import com.degrendel.wanbot.common.WanbotConfig
import com.degrendel.wanbot.grammar.AntlrGrammarProvider
import org.slf4j.LoggerFactory
import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.CommandLine.Option
import kotlin.system.exitProcess

private val L = LoggerFactory.getLogger("com.degrendel.wanbot.app.MainKt")!!

@Command(name = "Wanbot",
    version = ["WANBOT IS ETERNAL"],
    mixinStandardHelpOptions = true,
    description = ["THE REAL WANBOT IN THE HOUSE, BEACH"])
class WanbotCli : Runnable
{
  @Option(names = ["-p", "--profile"], description = ["Load the specified bundled profile configuration"])
  private var configProfile = ""

  @Option(names = ["-c", "--config"], description = ["Load the specified external overide configuration"])
  private var configOverride = ""

  @Option(names = ["--grammar-repl"], description = ["Launch the grammar REPL (does not start agent)"])
  private var grammarRepl = false

  override fun run()
  {
    Thread.setDefaultUncaughtExceptionHandler { _, e ->
      L.error("Uncaught Exception!", e)
      exitProcess(-1)
    }
    if (grammarRepl)
      launchGrammarRepl()
    else
      launchAgent()
  }

  private fun launchGrammarRepl()
  {
    L.info("STARTING GRAMMAR REPL")
    GrammarRepl().launch(AntlrGrammarProvider())
  }

  private fun launchAgent()
  {
    L.info("STARTING IN AGENT MODE")
    val grammar = AntlrGrammarProvider()
    val configuration = WanbotConfig(configProfile, configOverride)
    val session = WanbotSession(configuration, grammar)
    L.info("WORKING DIRECTORY IS {}", System.getProperty("user.dir"))
    session.launch()

  }
}

fun main(args: Array<String>) = CommandLine.run(WanbotCli(), *args)
