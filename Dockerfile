FROM openjdk:11-jre
LABEL version="0.0.1" description="WanBot is all up in yo container" maintainer="bryan@degrendel.com"
WORKDIR /wanbot
COPY wanbot-app-*.tar .

RUN tar xf wanbot-app-*.tar && rm wanbot-app-*.tar
RUN mv wanbot-app-*/* . && rmdir wanbot-app-*

ENV JAVA_TOOL_OPTIONS -agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n

ENTRYPOINT [ "./bin/wanbot-app" ]
CMD [ "--profile=docker" ]
